export function resolved(data) {
  console.info('resolved', data)
  return new Promise((resolve, reject) => {
    resolve({
      error: false,
      msg: 'OK',
      data: data,
    })
  })
}

export function rejected(msg) {
  return new Promise((resolve, reject) => {
    reject({
      error: true,
      msg: msg,
    })
  })
}
