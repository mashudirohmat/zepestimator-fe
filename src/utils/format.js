export function formatUang(angka) {
  const formatter = Intl.NumberFormat('id-ID')
  return formatter.format(ankg)
}

export function formatNumber(num) {
  const n = Number(num).toFixed(2)
  const numParts = n.toString().split('.')
  numParts[0] = numParts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  return numParts.join('.')
}
