// import menuPages from './menus/pages.menu'

export default {
  // main navigation - side menu
  menu: [{
    text: '',
    key: '',
    items: [
      { icon: 'mdi-graph', key: '', text: 'Estimator', 
        items: [
          { text: 'Prospect Project List', link: '/estimator' }
        ] 
      }
    ]
  }]

  // footer links
  // footer: [{
  //   text: 'Docs',
  //   key: 'menu.docs',
  //   href: 'https://vuetifyjs.com',
  //   target: '_blank'
  // }]
}
