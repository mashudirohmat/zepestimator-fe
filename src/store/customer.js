export const state = () => ({
  selectedProject: {},
  selectedQuotations: [],
  selectedQuotation: {},
  selectedAttachments: [],
  selectedAttachments: {},
  selectedTops: [],
  selectedTop: {},
  selectedJaminans: [],
  selectedJaminan: {},
  selectedBarangs: [],
  selectedBarang: {},
  selectedDetails: [],
  selectedDetail: {},
  selectedPekerjaans: [],
  selectedPekerjaan: {},
})

export const mutations = {
  SET_SELECTED_PROJECT(state, project) {
    state.selectedProject = project
  },
  SET_QUOTATIONS(state, quotations) {
    state.selectedQuotations = quotations
  },
  SET_SELECTED_QUOTATION(state, quotation) {
    state.selectedQuotation = quotation
  },
  SET_SELECTED_ATTACHMENTS(state, attachments) {
    state.selectedAttachments = attachments
  },
  SET_SELECTED_ATTACHMENT(state, attachment) {
    state.selectedAttachment = attachment
  },
  SET_SELECTED_TOPS(state, tops) {
    state.selectedTops = tops
  },
  SET_SELECTED_TOP(state, top) {
    state.selectedTop = top
  },
  SET_SELECTED_JAMINANS(state, jaminans) {
    state.selectedJaminans = jaminans
  },
  SET_SELECTED_JAMINAN(state, jaminan) {
    state.selectedJaminan = jaminan
  },
  SET_SELECTED_BARANGS(state, barangs) {
    state.selectedBarangs = barangs
  },
  SET_SELECTED_BARANG(state, barang) {
    state.selectedBarang = barang
  },
  SET_SELECTED_DETAILS(state, details) {
    state.selectedDetails = details
  },
  SET_SELECTED_DETAIL(state, detail) {
    state.selectedDetail = detail
  },
  SET_SELECTED_PEKERJAANS(state, pekerjaans) {
    state.selectedPekerjaans = pekerjaans
  },
  SET_SELECTED_PEKERJAAN(state, pekerjaan) {
    state.selectedPekerjaan = pekerjaan
  },
}

export const actions = {
  async getProjectsById({ commit }, id) {
    console.info('customer.actions.getProjectsById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const row = await this.$axios.$get(`/api/projects/${id}`)
    if (row.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: row.msg,
        })
      })
    } else {
      commit('SET_SELECTED_PROJECT', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getQuotations({ commit }) {
    console.info('customer.actions.getQuotations')
    const rows = await this.$axios.$get(`/api/quotations/customer_all`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_QUOTATIONS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getQuotationById({ commit }, id) {
    console.info('customer.actions.getQuotationById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const row = await this.$axios.$get(`/api/quotations/${id}`)
    if (row.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: row.msg,
        })
      })
    } else {
      commit('SET_SELECTED_QUOTATION', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  // attachments
  async getAttachmentsByQuotationId({ commit }, id) {
    console.info('customer.actions.getAttachmentsByQuotationId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/attachments/quotations/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_ATTACHMENTS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getAttachmentById({ commit }, id) {
    console.info('customer.actions.getAttachmentById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const row = await this.$axios.$get(`/api/attachments/${id}`)
    if (row.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: row.msg,
        })
      })
    } else {
      commit('SET_SELECTED_ATTACHMENT', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  // tops
  async getTopsByQuotationId({ commit }, id) {
    console.info('customer.actions.getTopsByQuotationId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/tops/quotations/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_TOPS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getTopById({ commit }, id) {
    console.info('customer.actions.getTopById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const row = await this.$axios.$get(`/api/tops/${id}`)
    if (row.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: row.msg,
        })
      })
    } else {
      commit('SET_SELECTED_TOP', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getJaminansByQuotationId({ commit }, id) {
    console.info('customer.actions.getJaminansByQuotationId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/jaminans/quotations/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_JAMINANS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getJaminanById({ commit }) {
    console.info('customer.actions.getJaminanById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const row = await this.$axios.$get(`/api/jaminans/${id}`)
    if (row.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: row.msg,
        })
      })
    } else {
      commit('SET_SELECTED_JAMINAN', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getBarangsByQuotationId({ commit }, id) {
    console.info('customer.actions.getBarangsByQuotationId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/barangs/quotation/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_BARANGS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getBarangById({ commit }, id) {
    console.info('customer.actions.getBarangById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const row = await this.$axios.$get(`/api/barangs/${id}`)
    if (row.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: row.msg,
        })
      })
    } else {
      commit('SET_SELECTED_BARANG', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getDetailsByBarangId({ commit }, id) {
    console.info('customer.actions.getDetailsByBarangId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/details/barang/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_DETAILS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getDetailById({ commit }, id) {
    console.info('customer.actions.getDetailById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const row = await this.$axios.$get(`/api/details/${id}`)
    if (row.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: row.msg,
        })
      })
    } else {
      commit('SET_SELECTED_DETAIL', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getPekerjaansByQuotationId({ commit }, id) {
    console.info('customer.actions.getPekerjaansByQuotationId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/pekerjaans/quotation/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_PEKERJAANS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getPekerjaanById({ commit }, id) {
    console.info('customer.actions.getPekerjaanById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const row = await this.$axios.$get(`/api/pekerjaans/${id}`)
    if (row.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: row.msg,
        })
      })
    } else {
      commit('SET_SELECTED_PEKERJAAN', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  /**
   * non getter from db
   */
  selectQuotation({ commit }, quotation) {
    commit('SET_SELECTED_QUOTATION', quotation)
  },
}
