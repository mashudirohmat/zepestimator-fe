import { resolved, rejected } from '../utils/promise'

export const state = () => ({
  // all approved rab
  selectedProject: {},
  selectedRabs: [],
  selectedRab: {},
  selectedWos: [],
  selectedWo: {},
  selectedSkos: [],
  selectedSko: {},
  selectedBarangs: [],
  selectedBarang: {},
  selectedRealisasis: [],
})

export const mutations = {
  SET_SELECTED_REALISASIS(state, realisasis) {
    state.selectedRealisasis = realisasis
  },
  SET_SELECTED_PROJECT(state, project) {
    state.selectedProject = project
  },
  SET_SELECTED_RABS(state, rabs) {
    state.selectedRabs = rabs
  },
  SET_SELECTED_RAB(state, rab) {
    state.selectedRab = rab
  },
  SET_SELECTED_WOS(state, wos) {
    state.selectedWos = wos
  },
  SET_SELECTED_WO(state, wo) {
    state.selectedWo = wo
  },
  SET_SELECTED_SKOS(state, skos) {
    state.selectedSkos = skos
  },
  RESET_SELECTED_SKOS(state) {
    state.selectedSkos = []
  },
  SET_SELECTED_SKO(state, sko) {
    state.selectedSko = sko
  },
  SET_SELECTED_BARANGS(state, barangs) {
    state.selectedBarangs = barangs
  },
  SET_SELECTED_BARANG(state, barang) {
    state.selectedBarang = barang
  },
}

export const actions = {
  async getRabById({ commit }, id) {
    console.info('wop.action.getRabById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    try {
      const row = await this.$axios.$get(`/api/rabs/${id}`)
      if (row.error) {
        throw {
          error: true,
          msg: row.msg,
        }
      }

      commit('SET_SELECTED_RAB', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: true,
          msg: 'OK',
          data: row.data,
        })
      })
    } catch (error) {
      return new Promise((resolve, reject) => {
        reject(error)
      })
    }
  },
  async getApprovedWos({ commit }) {
    console.info('wop.action.getApprovedWos')
    try {
      const rows = await this.$axios.$get(`/api/wops/approved`)
      if (rows.error) {
        throw {
          error: true,
          msg: rows.msg,
        }
      }

      commit('SET_SELECTED_WOS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
          data: rows.data,
        })
      })
    } catch (error) {
      return new Promise((resolve, reject) => {
        reject(error)
      })
    }
  },
  async getWoById({ commit }, id) {
    console.info('wop.action.getWoById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    try {
      const row = await this.$axios.$get(`/api/wops/${id}`)
      if (row.error) {
        throw {
          error: true,
          msg: row.msg,
        }
      }

      commit('SET_SELECTED_WO', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
          data: row.data,
        })
      })
    } catch (error) {
      return new Promise((resolve, reject) => {
        reject(error)
      })
    }
  },
  async getApprovedWo({ commit }) {
    console.info('wop.action.getApprovedWo')
    try {
      const row = await this.$axios.$get(`/api/wops/approved`)
      if (row.error) {
        throw row.msg
      }

      commit('SET_SELECTED_WOS', row.data)
      return resolved(row.data)
    } catch (error) {
      return rejected(error)
    }
  },
  async getSkosByWoId({ commit }, id) {
    console.info('wop.action.getSkosByWoId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    try {
      const rows = await this.$axios.$get(`/api/skos/wo/${id}`)
      if (rows.error) {
        throw {
          error: true,
          msg: rows.msg,
        }
      }

      commit('RESET_SELECTED_SKOS')
      commit('SET_SELECTED_SKOS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
          data: rows.data,
        })
      })
    } catch (error) {
      return new Promise((resolve, reject) => {
        reject(error)
      })
    }
  },
  async getSkoById({ commit }, id) {
    console.info('wop.action.getSkoById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    try {
      const row = await this.$axios.$get(`/api/skos/${id}`)
      if (row.error) {
        throw {
          error: true,
          msg: row.msg,
        }
      }

      commit('SET_SELECTED_SKO', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
          data: row.data,
        })
      })
    } catch (error) {
      return new Promise((resolve, reject) => {
        reject(error)
      })
    }
  },
  async getBarangsByWoId({ commit }, id) {
    console.log('wop.action.getBarangsByWoId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter barang tidak ditemukan',
        })
      })
    }

    try {
      const rows = await this.$axios.$get(`/api/wops/barangs/${id}`)
      if (rows.error) {
        throw {
          error: true,
          msg: rows.msg,
        }
      } else {
        commit('SET_SELECTED_BARANGS', rows.data)
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
            data: rows.data,
          })
        })
      }
    } catch (error) {
      return new Promise((resolve, reject) => {
        reject(error)
      })
    }
  },
  /**
   * non get data from server
   */
  selectWO({ commit }, wo) {
    commit('SET_SELECTED_WO', wo)
  },
  async addBarang({}, barang) {
    console.info('wop.action.addBarang', barang)
    console.info(barang)
    const { wop_id } = barang
    console.info('wop_id', wop_id)
    if (wop_id) {
      let bar = await this.$axios.$post(`/api/wops/add_barang/${wop_id}`, {
        barang: barang,
      })
      console.debug('bar', bar)
      if (bar) {
        if (bar.error) {
          return new Promise((resolve, reject) => {
            reject({
              error: true,
              msg: bar.msg,
            })
          })
        } else {
          return new Promise((resolve, reject) => {
            resolve({
              error: false,
              msg: 'OK',
            })
          })
        }
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Paramegter id  not found',
        })
      })
    }
  },
  async removeBarang({ commit }, id) {
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const d = await this.$axios.$delete(`/api/wops/remove_barang/${id}`)
    if (d.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: d.msg,
        })
      })
    } else {
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async addSko({}, sko) {
    console.info('wop.action.addSko', sko)
    if (sko) {
      let bar = await this.$axios.$post(`/api/skos`, {
        sko: sko,
      })
      console.debug('bar', bar)
      if (bar) {
        if (bar.error) {
          return new Promise((resolve, reject) => {
            reject({
              error: true,
              msg: bar.msg,
            })
          })
        } else {
          return new Promise((resolve, reject) => {
            resolve({
              error: false,
              msg: 'OK',
            })
          })
        }
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Paramegter id  not found',
        })
      })
    }
  },
  async removeSko({}, sko) {
    console.info('wop.action.removeSko', sko)
    if (!sko) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter sko tidak ditemukan',
        })
      })
    }

    try {
      const id = sko.id
      if (!id) {
        throw {
          error: true,
          msg: 'SKO id tidak ditemukan',
        }
      }

      let bar = await this.$axios.$delete(`/api/skos/${id}`)
      console.debug('bar', bar)
      if (bar) {
        if (bar.error) {
          throw {
            error: true,
            msg: bar.msg,
          }
        } else {
          return new Promise((resolve, reject) => {
            resolve({
              error: false,
              msg: 'OK',
            })
          })
        }
      }
    } catch (error) {
      return new Promise((resolve, reject) => {
        reject(error)
      })
    }
  },
  selectSKO({ commit }, sko) {
    commit('SET_SELECTED_SKO', sko)
  },
  async getBarangsBySkoId({ commit }, id) {
    console.log('wop.action.getBarangsBySkoId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter barang tidak ditemukan',
        })
      })
    }

    try {
      const rows = await this.$axios.$get(`/api/skos/barangs/${id}`)
      if (rows.error) {
        throw {
          error: true,
          msg: rows.msg,
        }
      } else {
        commit('SET_SELECTED_BARANGS', rows.data)
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
            data: rows.data,
          })
        })
      }
    } catch (error) {
      return new Promise((resolve, reject) => {
        reject(error)
      })
    }
  },
  async addBarangSko({}, barang) {
    console.info('wop.action.addBarangSko', barang)
    console.info(barang)
    const { sko_id } = barang
    console.info('wop_id', sko_id)
    if (sko_id) {
      let bar = await this.$axios.$post(`/api/skos/add_barang/${sko_id}`, {
        barang: barang,
      })
      console.debug('bar', bar)
      if (bar) {
        if (bar.error) {
          return new Promise((resolve, reject) => {
            reject({
              error: true,
              msg: bar.msg,
            })
          })
        } else {
          return new Promise((resolve, reject) => {
            resolve({
              error: false,
              msg: 'OK',
            })
          })
        }
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Paramegter id  not found',
        })
      })
    }
  },
  async removeBarangSko({ commit }, id) {
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    try {
      const d = this.$axios.$delete(`/api/skos/barang/${id}`)
      if (d.error) {
        throw {
          error: true,
          msg: 'Gagal hapus barang sko',
        }
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    } catch (error) {
      return new Promise((resolve, reject) => {
        reject(error)
      })
    }
  },

  async confirmSko({ commit }, id) {
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    try {
      const d = this.$axios.$put(`/api/skos/confirm/${id}`)
      if (d.error) {
        throw {
          error: true,
          msg: 'Gagal confirm SKO',
        }
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    } catch (error) {
      return new Promise((resolve, reject) => {
        reject(error)
      })
    }
  },
  async getProjectById({ commit }, id) {
    if (!id) {
      return rejected('Parameter id tidak ditemukan')
    }

    try {
      const p = await this.$axios.$get(`/api/projects/${id}`)
      if (p.error) {
        return rejected(p.msg)
      } else {
        commit('SET_SELECTED_PROJECT', p.data)
        return resolved(p.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async addRealisasiSko({ commit }, payload) {
    const { sko_id, qty, tipe } = payload

    if (!sko_id || !qty || !tipe) {
      return rejected('Parmeter tidak lengkap')
    }

    try {
      const u = await this.$axios.$post(`/api/skos/realisasi/${sko_id}`, payload)
      console.info('u action addRealiasiSko', u)
      if (u.error) {
        return rejected(u.msg)
      } else {
        return resolved(u.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async addRealisasiSkoBarang({ commit }, payload) {
    const { sko_id, qty, tipe, barang_id } = payload

    console.info('payload', payload)
    if (!sko_id || !qty || !tipe || !barang_id) {
      return rejected('Parmeter tidak lengkap')
    }

    try {
      const u = await this.$axios.$post(
        `/api/skos/realisasi_barang/${sko_id}`,
        payload
      )
      console.info('u action addRealisasiBarangSko', u)
      if (u.error) {
        return rejected(u.msg)
      } else {
        return resolved(u.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async getRealisasisBySkoId({ commit }, id) {
    if (!id) {
      return rejected('Parameter id tidak ditemukan')
    }

    try {
      const r = await this.$axios.$get(`/api/skos/realisasis/${id}`)
      if (r.error) {
        return rejected(r.msg)
      } else {
        commit('SET_SELECTED_REALISASIS', r.data)
        return resolved(r.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
}
