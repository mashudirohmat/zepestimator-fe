const master = require('../data/master')

export default () => ({
  user: 'Guest',
  sidebarOpen: false,
  selectedMenu: '',
  projects: [],
  masterData: master,
  showError: false,
  showSuccess: false,
  errorMessage: '',
  successMessage: '',

  selectedProject: {},
  selectedQuotation: {},
  selectedBarangHeader: {},
  selectedBarangDetail: {},
  selectedPekerjaan: {},

  // customer
  selectedQuotationCustomer: {},
})
