export const state = () => ({
  projects: [],
  selectedProject: {},
  selectedQuotation: {},
})

export const mutations = {
  SET_PROJECTS(state, projects) {
    state.projects = projects
  },
  SET_SELECTED_PROJECT(state, project) {
    state.selectedProject = project
  },
  SET_SELECTED_QUOTATION(state, quotation) {
    state.selectedQuotation = quotation
  },
}

export const actions = {
  async getProjects({ commit }) {
    console.info('management.actions.getProjects')
    const projects = await this.$axios.$get(`/projects/management`)
    if (projects.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: projects.msg,
        })
      })
    } else {
      commit('SET_PROJECTS', projects.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
          data: projects.data,
        })
      })
    }
  },
  async getProjectById({ commit }, id) {
    console.info('management.actions.getProjectById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const row = await this.$axios.$get(`/projects/${id}`)
    if (row.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: row.msg,
        })
      })
    } else {
      commit('SET_SELECTED_PROJECT', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  selectProject({ commit }, project) {
    commit('SET_SELECTED_PROJECT', project)
  },
  async getQuotationDeal({ commit }, project) {
    if (!project) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter project tidak ditemukan',
        })
      })
    }

    const id = project.id
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id project tidak ditemukan',
        })
      })
    }

    const quotation = await this.$axios.$get(`/quotations/deal/${id}`)
    if (quotation.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: quotation.msg,
        })
      })
    } else {
      commit('SET_SELECTED_QUOTATION', quotation.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
          data: quotation.data,
        })
      })
    }
  },
}
