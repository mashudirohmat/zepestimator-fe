export const state = () => ({
  db_tops: [],
  selectedTop: {},
  selectedTops: [],
})

export const mutations = {
  RESET_SELECTED_TOPS(state) {
    state.selectedTops = []
  },
  SET_SELECTED_TOPS(state, payload) {
    state.selectedTops = payload
  },
  ADD_TOP(state, top) {
    const topId = top.id
    let idx
    for (let i = 0; i < state.db_tops.length; i++) {
      const p = state.db_tops[i]
      if (p.id === topId) {
        idx = i
      }
    }

    if (idx === 0 || idx > 0) {
      console.log('Top sudah ada ')
    } else {
      state.db_tops.push(top)
    }
  },
  UPDATE_TOP(state, top) {
    const topId = top.id

    let idx
    for (let i = 0; i < state.db_tops.length; i++) {
      const p = state.db_tops[i]
      if (p.id === topId) {
        idx = i
      }
    }

    state.db_tops.splice(idx, 1, top)
  },
  REMOVE_TOP(state, top) {
    const topId = top.id

    let idx
    for (let i = 0; i < state.db_tops.length; i++) {
      const p = state.db_tops[i]
      if (p.id === topId) {
        idx = i
      }
    }

    state.db_tops.splice(idx, 1)
  },
  SELECT_TOP(state, top) {
    state.selectedTop = top
  },
  CLEAR_SELECTED_TOP(state) {
    state.selectedTop = {}
  },
}

export const actions = {
  /**
   * get terms of payment global state top.selectedTops
   * using quotation id
   * return promise
   * @param {*} param0
   * @param {*} id
   */
  async getSelectedTops({ commit }, id) {
    console.info('tops.action.getSelectedTops', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan !',
        })
      })
    } else {
      commit('RESET_SELECTED_TOPS')

      const tops = await this.$axios.$get(`/api/tops/quotation/${id}`)

      if (tops.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: tops.msg,
          })
        })
      } else {
        commit('SET_SELECTED_TOPS', tops.data)
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  setSelectedTops({ commit }, payload) {
    commit('SET_SELECTED_TOPS', payload)
  },
  /**
   * add new top
   * return promise
   * @param {*} param0
   * @param {*} top
   */
  async addTop({ commit }, top) {
    if (!top) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter top tidak ditemukan ',
        })
      })
    } else {
      const result = await this.$axios.$post('/api/tops', top)
      if (result.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: result.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  updateTop({ commit }, top) {
    commit('UPDATE_TOP', top)
  },
  /**
   * remove top
   * return promise
   * @param {*} param0
   * @param {*} top
   */
  async removeTop({ commit }, top) {
    if (!top) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter top tidak ditemukan ',
        })
      })
    } else {
      const d = await this.$axios.$delete(`/api/tops/${top.id}`)
      if (d.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: d.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  selectTop({ commit }, top) {
    commit('SELECT_TOP', top)
  },
}
