export const state = () => ({
  projects: [],
  selectedProjects: [],
  selectedProject: {},
  quotations: [],
  selectedQuotations: [],
  selectedQuotation: {},
  selectedAttachments: [],
  selectedAttachment: {},
  selectedTops: [],
  selectedTop: {},
  selectedJaminans: [],
  selectedJaminan: {},
  selectedBarangs: [],
  selectedBarang: {},
  selectedDetails: [],
  selectedDetail: {},
  selectedPekerjaans: [],
  selectedPekerjaan: {},
})

export const mutations = {
  SET_PROJECTS(state, projects) {
    state.projects = projects
  },
  SET_SELECTED_PROJECTS(state, projects) {
    state.selectedProjects = projects
  },
  SET_SELECTED_PROJECT(state, project) {
    state.selectedProject = project
  },
  SET_QUOTATIONS(state, quotations) {
    state.quotations = quotations
  },
  SET_SELECTED_QUOTATIONS(state, quotations) {
    state.selectedQuotations = quotations
  },
  SET_SELECTED_QUOTATION(state, quotation) {
    state.selectedQuotation = quotation
  },
  SET_SELECTED_ATTACHMENTS(state, attachments) {
    state.selectedAttachments = attachments
  },
  SET_SELECTED_ATTACHMENT(state, attachment) {
    state.selectedAttachment = attachment
  },
  SET_SELECTED_TOPS(state, tops) {
    state.selectedTops = tops
  },
  SET_SELECTED_TOP(state, top) {
    state.selectedTop = top
  },
  SET_SELECTED_JAMINANS(state, jaminans) {
    state.selectedJaminans = jaminans
  },
  SET_SELECTED_JAMINAN(state, jaminan) {
    state.selectedJaminan = jaminan
  },
  SET_SELECTED_BARANGS(state, barangs) {
    state.selectedBarangs = barangs
  },
  SET_SELECTED_BARANG(state, barang) {
    state.selectedBarang = barang
  },
  SET_SELECTED_DETAILS(state, details) {
    state.selectedDetails = details
  },
  SET_SELECTED_DETAIL(state, detail) {
    state.selectedDetail = detail
  },
  SET_SELECTED_PEKERJAANS(state, pekerjaans) {
    state.selectedPekerjaans = pekerjaans
  },
  SET_SELECTED_PEKERJAAN(state, pekerjaan) {
    state.selectedPekerjaan = pekerjaan
  },
}

export const actions = {
  // projects
  async getAllProjects({ commit }) {
    console.info('estimator.actions.getAllProjects')
    const rows = await this.$axios.$get(`/api/projects`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_PROJECTS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getProjectsDrafNego({ commit }) {
    console.info('estimator.actions.getProjectsDrafNego', status)

    const rows = await this.$axios.$get(`/api/projects/estimator`)
    console.info('rows', rows)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_PROJECTS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getProjectsByStatus({ commit }, status) {
    console.info('estimator.actions.getProjectsByStatus', status)
    if (!status) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter status tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/projects/status/${status}`)
    console.info('rows', rows)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_PROJECTS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getProjectsById({ commit }, id) {
    console.info('estimator.actions.getProjectsById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const row = await this.$axios.$get(`/api/projects/${id}`)
    if (row.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: row.msg,
        })
      })
    } else {
      commit('SET_SELECTED_PROJECT', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  // quotations
  async getAllQuotations({ commit }) {
    console.info('estimator.actions.getAllQuotations')
    const rows = await this.$axios.$get(`/api/quotations`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_QUOTATIONS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getQuotationsByProjectId({ commit }, id) {
    console.info('estimator.actions.getQuotationsByProjectId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/quotations/project/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_QUOTATIONS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getQuotationById({ commit }, id) {
    console.info('estimator.actions.getQuotationById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const row = await this.$axios.$get(`/api/quotations/${id}`)
    if (row.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: row.msg,
        })
      })
    } else {
      commit('SET_SELECTED_QUOTATION', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  // attachments
  async getAttachmentsByQuotationId({ commit }, id) {
    console.info('estimator.actions.getAttachmentsByQuotationId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/attachments/quotations/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_ATTACHMENTS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getAttachmentById({ commit }, id) {
    console.info('estimator.actions.getAttachmentById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const row = await this.$axios.$get(`/api/attachments/${id}`)
    if (row.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: row.msg,
        })
      })
    } else {
      commit('SET_SELECTED_ATTACHMENT', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  // tops
  async getTopsByQuotationId({ commit }, id) {
    console.info('estimator.actions.getTopsByQuotationId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/tops/quotations/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_TOPS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getTopById({ commit }, id) {
    console.info('estimator.actions.getTopById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const row = await this.$axios.$get(`/api/attachments/${id}`)
    if (row.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: row.msg,
        })
      })
    } else {
      commit('SET_SELECTED_TOP', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getJaminansByQuotationId({ commit }, id) {
    console.info('estimator.actions.getJaminansByQuotationId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/jaminans/quotations/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_JAMINANS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getJaminanById({ commit }) {
    console.info('estimator.actions.getJaminanById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const row = await this.$axios.$get(`/api/jaminans/${id}`)
    if (row.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: row.msg,
        })
      })
    } else {
      commit('SET_SELECTED_TOP', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getBarangsByQuotationId({ commit }, id) {
    console.info('estimator.actions.getBarangsByQuotationId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/barangs/quotations/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_BARANGS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getBarangById({ commit }, id) {
    console.info('estimator.actions.getBarangById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const row = await this.$axios.$get(`/api/barangs/${id}`)
    if (row.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: row.msg,
        })
      })
    } else {
      commit('SET_SELECTED_BARANG', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getDetailsByBarangId({ commit }, id) {
    console.info('estimator.actions.getDetailsByBarangId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/details/barang/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_DETAILS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
          data: rows.data,
        })
      })
    }
  },
  async getDetailById({ commit }, id) {
    console.info('estimator.actions.getDetailById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const row = await this.$axios.$get(`/api/details/${id}`)
    if (row.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: row.msg,
        })
      })
    } else {
      commit('SET_SELECTED_DETAIL', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getPekerjaansByQuotationId({ commit }, id) {
    console.info('estimator.actions.getPekerjaansByQuotationId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/pekerjaans/quotations/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_PEKERJAANS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getPekerjaanById({ commit }, id) {
    console.info('estimator.actions.getPekerjaanById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const row = await this.$axios.$get(`/api/pekerjaans/${id}`)
    if (row.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: row.msg,
        })
      })
    } else {
      commit('SET_SELECTED_PEKERJAAN', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  /**
   * actions for non setters
   */
  async deleteProject({ commit, dispatch }, id) {
    console.info(`estimator.action.deleteProject`, id)
    if (id) {
      const d = await this.$axios.$delete(`/api/projects/${id}`)
      if (d.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: d.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }
  },
  async addProject({ dispatch }, project) {
    console.info('project.action.addProject', project)
    if (project) {
      const result = await this.$axios.$post('/api/projects', project)
      if (result.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: result.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter project tidak ditemukan',
        })
      })
    }
  },
  selectProject({ commit }, project) {
    commit('SET_SELECTED_PROJECT', project)
  },
  selectQuotation({ commit }, quotation) {
    commit('SET_SELECTED_QUOTATION', quotation)
  },
  async addQuotation({}, quotation) {
    if (quotation) {
      const result = await this.$axios.$post('/api/quotations', quotation)
      if (result.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: result.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
            data: result.data,
          })
        })
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter quotation tidak ditemukan',
        })
      })
    }
  },
  async deleteQuotationById({ commit }, id) {
    console.info(`estimator.action.deleteQuotationById`, id)
    if (id) {
      const d = await this.$axios.$delete(`/api/quotations/${id}`)
      if (d.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: d.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }
  },
  async addAttachment({ commit }, attachment) {
    if (attachment) {
      const result = await this.$axios.$post('/api/attachments', attachment)
      if (result.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: result.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter attachment tidak ditemukan',
        })
      })
    }
  },
  async removeAttachment({ commit }, attachment) {
    if (attachment) {
      const d = await this.$axios.$delete(`/api/attachments/${attachment.id}`)
      if (d.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: d.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter attachment tidak ditemukan',
        })
      })
    }
  },
  async addTop({ commit }, top) {
    if (!top) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter top tidak ditemukan ',
        })
      })
    } else {
      const result = await this.$axios.$post('/api/tops', top)
      if (result.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: result.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  async removeTop({ commit }, top) {
    if (!top) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter top tidak ditemukan ',
        })
      })
    } else {
      const id = top.id
      if (!id) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: 'Parameter id term of payment tidak ditemukan',
          })
        })
      }

      const d = await this.$axios.$delete(`/api/tops/${id}`)
      if (d.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: d.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  // jaminans
  async addJaminan({ commit }, jaminan) {
    if (!jaminan) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter jamian tidak ditemukan',
        })
      })
    } else {
      const result = await this.$axios.$post('/api/jaminans', jaminan)
      if (result.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: result.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  async removeJaminan({ commit }, jaminan) {
    if (!jaminan) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter jaminan not found',
        })
      })
    } else {
      const id = jaminan.id
      if (!id) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: 'Parameter id jaminan not found',
          })
        })
      }

      const d = await this.$axios.$delete(`/api/jaminans/${id}`)
      if (d.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: d.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  // pekerjaans
  async addPekerjaan({ dispatch }, pekerjaan) {
    if (!pekerjaan) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter pekerjaan tidak ditemukan',
        })
      })
    } else {
      const ad = await this.$axios.$post('/api/pekerjaans', pekerjaan)
      if (ad.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: 'Parameter pekerjaan tidak ditemukan',
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  async removePekerjaan({ commit }, pekerjaan) {
    if (pekerjaan) {
      const id = pekerjaan.id
      if (!id) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: 'Parameter id pekerjaan tidak ditemukan',
          })
        })
      }

      const d = await this.$axios.$delete(`/api/pekerjaans/${pekerjaan.id}`)
      if (d.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: d.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter barang tidak ditemukan ',
        })
      })
    }
  },
  // potongan
  async updatePotongan({ commit }, payload) {
    const { id, potongan } = payload
    console.info('estimator.action.updatePotongan', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        console.info('updatePotongan')
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    } else {
      const ud = await this.$axios.$put(`/api/quotations/updatePotongan/${id}`, {
        potongan: potongan,
      })
      if (ud.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: ud.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'ok',
          })
        })
      }
    }
  },
  async updateTax({ commit }, payload) {
    const { id, tax } = payload
    console.info('estimator.action.updateTax', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        console.info('updateTax')
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    } else {
      const ud = await this.$axios.$put(`/api/quotations/updateTax/${id}`, {
        tax: tax,
      })
      if (ud.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: ud.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'ok',
          })
        })
      }
    }
  },
  // barang
  async addBarang({ commit }, barang) {
    console.info(`barang.action.addBarang()`, barang)
    if (!barang) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter barang tidak ditemukan',
        })
      })
    } else {
      const ad = await this.$axios.$post('/api/barangs', barang)
      if (ad.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: ad.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  async removeBarang({ commit }, id) {
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    } else {
      const d = await this.$axios.$delete(`/api/barangs/${id}`)
      if (d.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: d.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  async addDetail({ commit }, barang) {
    if (!barang) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter barang tidak ditemukan ',
        })
      })
    } else {
      const ad = await this.$axios.$post('/api/details', barang)
      if (ad.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: ad.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  async removeDetail({ commit }, barang) {
    console.info('estimator.action.removeDetail', barang)
    if (barang) {
      const d = await this.$axios.$delete(`/api/details/${barang.id}`)
      if (d.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: d.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter barang tidak ditemukan ',
        })
      })
    }
  },
  async kirimPenawaranKeCustomer({ commit }, payload) {
    const { id, quotation } = payload
    console.info('quotation.action.kirimPenawaran', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        console.info('kirimPenawaranKeCustomer')
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    } else {
      const ud = await this.$axios.$put(`/api/quotations/kirimPenawaran/${id}`, {
        quotation: quotation,
      })
      if (ud.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: ud.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  async closeManual({ commit }, payload) {
    const { id, quotation } = payload
    console.info('quotation.action.closeManual', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        console.info('closeManual')
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    } else {
      const ud = await this.$axios.$put(`/api/quotations/closeManual/${id}`, {
        quotation: quotation,
      })
      if (ud.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: ud.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  async updateNote({ commit }, payload) {
    const { id, note } = payload
    if (id) {
      return await this.$axios.$put(`/api/quotations/updateNote/${id}`, {
        note: note,
      })
    } else {
      return new Promise((resolve, reject) => {
        console.info('updateNote')
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }
  },
  async kirimPenawaranKeVendor({ commit }, payload) {
    const { id, quotation } = payload
    if (id) {
      return await this.$axios.$put(`/api/quotations/kirimJawaban/${id}`, {
        quotation: quotation,
      })
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }
  },
  async setujuPenawaran({ commit }, payload) {
    const { id, quotation } = payload
    console.info('quotation.action.closeManual', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        console.info('closeManual')
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    } else {
      const ud = await this.$axios.$put(`/api/quotations/setujuPenawaran/${id}`, {
        quotation: quotation,
      })
      if (ud.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: ud.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  async getProjectsManagement({ commit }) {
    console.info('estimator.actions.getProjectsManagement')
    const projects = await this.$axios.$get(`/api/projects/management`)
    if (projects.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: projects.msg,
        })
      })
    } else {
      commit('SET_SELECTED_PROJECTS', projects.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
          data: projects.data,
        })
      })
    }
  },
  async getQuotationDeal({ commit }, project) {
    if (!project) {
      return new Promise((_resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter project tidak ditemukan',
        })
      })
    }

    const id = project.id
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id project tidak ditemukan',
        })
      })
    }

    const quotation = await this.$axios.$get(`/api/quotations/deal/${id}`)
    if (quotation.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: quotation.msg,
        })
      })
    } else {
      commit('SET_SELECTED_QUOTATION', quotation.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
          data: quotation.data,
        })
      })
    }
  },
  async updateMargin({ commit, dispatch }, payload) {
    console.info('updateMargin', payload)
    const { id, margin } = payload
    if (id) {
      const m = await this.$axios.$put(`/api/projects/update_margin/${id}`, {
        margin: margin,
      })
      if (m.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: m.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id project tidak ditemukan',
        })
      })
    }
  },
  async updateRetensi({ commit, dispatch }, payload) {
    const { id, retensi } = payload
    if (id) {
      const r = await this.$axios.$put(`/api/projects/update_retensi/${id}`, {
        retensi: retensi,
      })
      if (r.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: r.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id project tidak ditemukan',
        })
      })
    }
  },
  async updatePajakRetensi({ commit, dispatch }, payload) {
    const { id, pajak } = payload
    if (id) {
      const p = await this.$axios.$put(`/api/projects/update_pajak_retensi/${id}`, {
        pajak: pajak,
      })
      if (p.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: p.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id project tidak ditemukan',
        })
      })
    }
  },
  async konfirmMargin({}, id) {
    if (id) {
      const c = await this.$axios.$put(`/api/projects/confirm_margin/${id}`, {
        confirm: 'OK',
      })
      if (c.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: c.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id project tidak ditemukan',
        })
      })
    }
  },
}
