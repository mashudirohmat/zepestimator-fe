export const state = () => ({
  selectedProjects: [],
  selectedProject: {},
  selectedQuotations: [],
  selectedQuotation: {},
  selectedRabs: [],
  selectedRab: {},
  selectedBarangs: [],
  selectedBarang: {},
  selectedDetails: [],
  selectedDetail: {},
  selectedWos: [],
  selectedWo: {},
  selectedBarangsWo: [],
  selectedDetailsWo: [],
})

export const mutations = {
  SET_SELECTED_PROJECTS(state, projects) {
    state.selectedProjects = projects
  },
  SET_SELECTED_PROJECT(state, project) {
    state.selectedProject = project
  },
  SET_SELECTED_QUOTATIONS(state, quotations) {
    state.selectedQuotations = quotations
  },
  SET_SELECTED_QUOTATION(state, quotation) {
    state.selectedQuotation = quotation
  },
  SET_SELECTED_RABS(state, rabs) {
    state.selectedRabs = rabs
  },
  SET_SELECTED_RAB(state, rab) {
    state.selectedRab = rab
  },
  SET_SELECTED_BARANGS(state, barangs) {
    state.selectedBarangs = barangs
  },
  SET_SELECTED_BARANG(state, barang) {
    state.selectedBarang = barang
  },
  SET_SELECTED_DETAILS(state, details) {
    state.selectedDetails = details
  },
  SET_SELECTED_DETAIL(state, detail) {
    state.selectedDetail = detail
  },
  SET_SELECTED_WOS(state, wos) {
    state.selectedWos = wos
  },
  SET_SELECTED_WO(state, wo) {
    state.selectedWo = wo
  },
  SET_SELECTED_BARANGS_WO(state, barangs) {
    state.selectedBarangsWo = barangs
  },
  SET_SELECTED_DETAILS_WO(state, details) {
    state.selectedDetailsWo = details
  },
}

export const actions = {
  async getProjectsForRAB({ commit }) {
    console.info('engineering.action.getProjectsForRAB')
    const rows = await this.$axios.$get(`/api/projects/engineering/projects`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_PROJECTS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getProjectsForPPIC({ commit }) {
    console.info('engineering.action.getProjectsForRAB')
    const rows = await this.$axios.$get(`/api/projects/ppic`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_PROJECTS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getProjectsApproved({ commit }) {
    console.info('engineering.action.getProjectsApproved')
    const rows = await this.$axios.$get(`/api/projects/approved`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_PROJECTS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getProjectById({ commit }, id) {
    console.info('engineering.action.getProjectById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan !',
        })
      })
    }

    const row = await this.$axios.$get(`/api/projects/${id}`)
    if (row.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: row.msg,
        })
      })
    } else {
      commit('SET_SELECTED_PROJECT', row.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getQuotationsByProjectId({ commit }, id) {
    console.info('engineering.action.getQuotationsByProjectId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/quotations/project/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_QUOTATIONS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getQuotationById({ commit }, id) {
    console.info('engineering.action.getQuotationBytId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/quotations/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_QUOTATION', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getRabsDraf({ commit }) {
    console.info('engineering.action.getRabsDraf')
    const rows = await this.$axios.$get(`/api/rabs/draf`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_RABS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getRabById({ commit }, id) {
    console.info('engineering.action.getRabById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/rabs/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_RAB', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getRabByProjectId({ commit }, id) {
    console.info('engineering.action.getRabByProjectId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/rabs/project/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_RAB', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
          data: rows.data,
        })
      })
    }
  },
  async getBarangsByRabId({ commit }, id) {
    console.info('engineering.action.getBarangsByRabId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/rabs/barangs/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_BARANGS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getBarangById({ commit }, id) {
    console.info('engineering.action.getBarangById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/rabs/barang/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_BARANG', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  async getDetailsByBarangId({ commit }, id) {
    console.info('engineering.action.getDetailsByBarangId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/rabs/details/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_DETAILS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
          data: rows.data,
        })
      })
    }
  },
  async getDetailById({ commit }, id) {
    console.info('engineering.action.getDetailById', id)
    console.info('getDetailById', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/rabs/detail/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_DETAIL', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },

  /**
   * non get from db
   */
  selectProject({ commit }, project) {
    console.info('engineering.action.selectProject', project)
    commit('SET_SELECTED_PROJECT', project)
  },
  async updateDetailBarang({ dispatch }, barang) {
    console.info('engineering.action.updateDetailBarang', barang)
    if (barang) {
      try {
        const id = barang.id
        const rab_id = barang.rab_id
        console.info('id', id)
        console.info('rab_id', rab_id)
        let u = await this.$axios.$put(`/api/rabs/update_detail/${id}`, {
          barang: barang,
        })
        if (u.error) {
          throw u.msg
        }

        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      } catch (error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: error,
          })
        })
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter barang not found',
        })
      })
    }
  },
  async removeBarang({ commit, dispatch }, payload) {
    console.info('engineering.action.removeBarang', payload)
    const { id, rab_id } = payload
    console.info('engineering.action.removeBarang')
    if (id) {
      let rem = await this.$axios.$delete(`/api/rabs/remove_barang/${id}`)
      if (rem) {
        if (rem.error) {
          return new Promise((resolve, reject) => {
            reject({
              error: true,
              msg: rem.msg,
            })
          })
        } else {
          commit('SET_SELECTED_RAB', rem.data)
          dispatch('getBarangsByRabId', rab_id)
          return new Promise((resolve, reject) => {
            resolve({
              error: false,
              msg: 'Deleted',
            })
          })
        }
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter tidak ditemukan',
        })
      })
    }
  },
  async addBarang({ commit, dispatch }, barang) {
    console.info('engineering.action.addBarang', barang)
    console.info(barang)
    const { rab_id } = barang
    console.info('rab_id', rab_id)
    if (rab_id) {
      let bar = await this.$axios.$post(`/api/rabs/barang/${rab_id}`, {
        barang: barang,
      })
      console.debug('bar', bar)
      if (bar) {
        if (bar.error) {
          return new Promise((resolve, reject) => {
            reject({
              error: true,
              msg: bar.msg,
            })
          })
        } else {
          return new Promise((resolve, reject) => {
            resolve({
              error: false,
              msg: 'OK',
            })
          })
        }
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Paramegter id  not found',
        })
      })
    }
  },
  async updateBarangHeader({ commit, dispatch }, header) {
    console.info('engineering.action.updateBarangHeader', header)
    console.info(header)
    if (header) {
      const rabId = header.rab_id
      const id = header.id
      console.info('rabId', rabId, id)
      try {
        let upd = await this.$axios.$put(`/api/rabs/update_barang_header/${id}`, {
          barang: header,
        })
        if (upd.error) {
          return new Promise((resolve, reject) => {
            reject({
              error: true,
              msg: upd.error,
            })
          })
        }

        /**
         * proses lanjutan setelah update header adalah
         * reload selectedRab -> ok
         * reload selectedBarangs => ok
         * return dari proses update adalah rab object
         */
        console.info('upd.data', upd)
        commit('SET_SELECTED_RAB', upd.data)
        await dispatch('getBarangsByRabId', rabId)

        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      } catch (error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: error,
          })
        })
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter tidak ditemukan ',
        })
      })
    }
  },

  async kirimRabKePPIC({ commit }, payload) {
    const { id, note } = payload
    if (id) {
      let kirim = await this.$axios.$put(`/api/rabs/approve_rab/${id}`, {
        note: note,
      })
      if (kirim.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: kirim.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'ok',
          })
        })
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Data tidak ditemukan',
        })
      })
    }
  },
  async kirimWOProduksi({ commit }, payload) {
    const { tanggal, note, id } = payload
    if (id) {
      let kirim = await this.$axios.$put(`/api/rabs/kirim_wo/${id}`, {
        note: note,
        tanggal: tanggal,
      })
      if (kirim.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: kirim.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'ok',
          })
        })
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Data tidak ditemukan',
        })
      })
    }
  },
  async getRabDetailsByBarangId({ commit }, id) {
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Paramter id barang tidak ditemukan',
        })
      })
    }

    const barangs = await this.$axios.$get(`/api/rabs/details/${id}`)
    if (barangs.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: barangs.msg,
        })
      })
    } else {
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
          data: barangs.data,
        })
      })
    }
  },
  async addBarang({ commit }, barang) {
    console.info(`engineering.action.addBarang()`, barang)
    if (!barang) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter barang tidak ditemukan',
        })
      })
    } else {
      const id = barang.rab_id
      if (!id) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: 'Parameter id barang tidak ditemukan',
          })
        })
      }
      const ad = await this.$axios.$post(`/api/rabs/barang/${id}`, {
        barang: barang,
      })
      if (ad.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: ad.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  async removeBarang({ commit }, id) {
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    } else {
      const d = await this.$axios.$delete(`/api/rabs/remove_barang/${id}`)
      if (d.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: d.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  async addDetail({ commit }, barang) {
    if (!barang) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter barang tidak ditemukan ',
        })
      })
    } else {
      const id = barang.parent_id
      if (!id) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: 'Parameter id barang tidak ditemukan ',
          })
        })
      }
      const ad = await this.$axios.$post(`/api/rabs/detail/${id}`, {
        barang: barang,
      })
      if (ad.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: ad.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  async removeDetail({ commit }, id) {
    console.info('engineering.action.removeDetail', id)
    if (id) {
      const d = await this.$axios.$delete(`/api/rabs/remove_detail/${id}`)
      if (d.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: d.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter barang tidak ditemukan ',
        })
      })
    }
  },
  // production
  async getProjects({ commit }) {
    const rows = await this.$axios.$get(`/api/projects/wo`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_PROJECTS', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
          data: rows.data,
        })
      })
    }
  },
  async getWOByRabId({ commit }, id) {
    console.info('id', !id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    console.info('getWOByRabId id', `/api/wops/wo/${id}`)
    const wo = await this.$axios.$get(`/api/wops/wo/${id}`)
    console.log('wo', wo)
    if (wo.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: wo.msg,
        })
      })
    } else {
      commit('SET_SELECTED_WO', wo.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
          data: wo.data,
        })
      })
    }
  },
  async addBarangWO({ commit }, barang) {
    console.info(`engineering.action.addBarangWO()`, barang)
    if (!barang) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter barang tidak ditemukan',
        })
      })
    } else {
      const id = barang.wop_id
      if (!id) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: 'Parameter id barang tidak ditemukan',
          })
        })
      }
      const ad = await this.$axios.$post(`/api/wops/add_barang/${id}`, {
        barang: barang,
      })
      if (ad.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: ad.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  async getBarangsByWoId({ commit }, id) {
    console.info('engineering.action.getBarangsByWoId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/wops/barangs/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      commit('SET_SELECTED_BARANGS_WO', rows.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
          data: rows.data,
        })
      })
    }
  },
  async getWoDetailsByBarangId({ commit }, id) {
    console.info('engineering.action.getWoDetailsByBarangId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const rows = await this.$axios.$get(`/api/wops/details/${id}`)
    if (rows.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: rows.msg,
        })
      })
    } else {
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
          data: rows.data,
        })
      })
    }
  },
  async removeBarangWo({ commit, dispatch }, payload) {
    console.info('engineering.action.removeBarangWo', payload)
    const { id, wo_id } = payload
    console.info('engineering.action.removeBarang', wo_id)
    if (id) {
      let rem = await this.$axios.$delete(`/api/wops/remove_barang/${id}`)
      if (rem) {
        if (rem.error) {
          return new Promise((resolve, reject) => {
            reject({
              error: true,
              msg: rem.msg,
            })
          })
        } else {
          return new Promise((resolve, reject) => {
            resolve({
              error: false,
              msg: 'Deleted',
            })
          })
        }
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter tidak ditemukan',
        })
      })
    }
  },
  async confirmWO({ commit }, payload) {
    const { id, note } = payload
    console.info('engineering.action.confirmWO', id)
    if (id) {
      let rem = await this.$axios.$put(`/api/wops/confirm_wo/${id}`, {
        note: note,
      })
      if (rem) {
        if (rem.error) {
          return new Promise((resolve, reject) => {
            reject({
              error: true,
              msg: rem.msg,
            })
          })
        } else {
          return new Promise((resolve, reject) => {
            resolve({
              error: false,
              msg: 'Deleted',
            })
          })
        }
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter tidak ditemukan',
        })
      })
    }
  },
  async getWosByProjectId({ commit }, id) {
    console.info('engineering.action.getWosByProjectId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    try {
      const wos = await this.$axios.$get(`/api/wops/project/${id}`)
      if (wos.error) {
        throw {
          error: true,
          msg: wos.msg,
        }
      } else {
        commit('SET_SELECTED_WOS', wos.data)
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
            data: wos.data,
          })
        })
      }
    } catch (error) {
      return new Promise((resolve, reject) => {
        reject(error)
      })
    }
  },
  async getWosByRabId({ commit }, id) {
    console.info('engineering.action.getWosByRabId', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    try {
      const wos = await this.$axios.$get(`/api/wops/wo/${id}`)
      if (wos.error) {
        throw {
          error: true,
          msg: wos.msg,
        }
      } else {
        commit('SET_SELECTED_WO', wos.data)
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
            data: wos.data,
          })
        })
      }
    } catch (error) {
      return new Promise((resolve, reject) => {
        reject(error)
      })
    }
  },
  async updateHargaRab({ commit }, payload) {
    const { id, harga, total } = payload
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    const u = this.$axios.$put(`/api/rabs/update_harga/${id}`, {
      harga: harga,
      total: total,
    })

    if (u.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: u.msg,
        })
      })
    } else {
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
          data: u.data,
        })
      })
    }
  },
}
