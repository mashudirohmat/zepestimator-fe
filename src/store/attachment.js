export const state = () => ({
  db_attachments: [],
  selectedAttachment: {},
  selectedAttachments: [],
})

export const mutations = {
  SET_SELECTED_ATTACHMENTS(state, payload) {
    state.selectedAttachments = payload
  },
  RESET_SELECTED_ATTACHMENTS(state) {
    state.selectedAttachments = []
  },
  SET_SELECTED_ATTACHMENTS(state, payload) {
    state.selectedAttachments = payload
  },
  ADD_ATTACHMENT(state, attachment) {
    const attachmentId = attachment.id
    let idx
    for (let i = 0; i < state.db_attachments.length; i++) {
      const p = state.db_attachments[i]
      if (p.id === attachmentId) {
        idx = i
      }
    }

    if (idx === 0 || idx > 0) {
      console.log('Attachment sudah ada ')
    } else {
      state.db_attachments.push(attachment)
    }
  },
  UPDATE_ATTACHMENT(state, attachment) {
    const attachmentId = attachment.id

    let idx
    for (let i = 0; i < state.db_attachments.length; i++) {
      const p = state.db_attachments[i]
      if (p.id === attachmentId) {
        idx = i
      }
    }

    state.db_attachments.splice(idx, 1, attachment)
  },
  REMOVE_ATTACHMENT(state, attachment) {
    const attachmentId = attachment.id
    console.log('remove doc', attachmentId)

    let idx
    for (let i = 0; i < state.db_attachments.length; i++) {
      const p = state.db_attachments[i]
      if (p.id === attachmentId) {
        idx = i
      }
    }

    console.log('idx', idx)
    state.db_attachments.splice(idx, 1)
  },
  SELECT_ATTACHMENT(state, attachment) {
    state.selectedAttachment = attachment
  },
  CLEAR_SELECTED_ATTACHMENT(state) {
    state.selectedAttachment = {}
  },
}

export const actions = {
  /**
   * get selected attachment by quotation id
   * return promise
   * @param {*} param0
   * @param {*} id
   */
  async getSelectedAttachments({ commit }, id) {
    console.info(`attachment.action.getSelectedAttachments`, id)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan ',
        })
      })
    }

    commit('RESET_SELECTED_ATTACHMENTS')
    const ats = await this.$axios.$get(`/api/attachments/quotation/${id}`)

    if (ats.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: ats.msg,
        })
      })
    } else {
      commit('SET_SELECTED_ATTACHMENTS', ats.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  /**
   * add new attachment document
   * @param {*} param0
   * @param {*} attachment
   */
  async addAttachment({ commit }, attachment) {
    if (attachment) {
      const result = await this.$axios.$post('/api/attachments', attachment)
      if (result.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: result.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  setSelectedAttachments({ commit }, payload) {
    commit('SET_SELECTED_ATTACHMENTS', payload)
  },
  updateAttachment({ commit }, attachment) {
    commit('UPDATE_ATTACHMENT', attachment)
  },
  async removeAttachment({ commit }, attachment) {
    if (attachment) {
      return await this.$axios.$delete(`/api/attachments/${attachment.id}`)
    }
  },
  selectAttachment({ commit }, attachment) {
    commit('SELECT_ATTACHMENT', attachment)
  },
}
