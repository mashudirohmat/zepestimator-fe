import { resolved, rejected } from '../utils/promise'

export const state = () => ({
  db_projects: [],
  /**
   * global state
   * list of project for selected function
   * such as approved, draf, etc
   */
  selectedProjects: [],
  selectedProject: {},
  selectedProjectManagement: {},
  selectedProjectPpic: {},
  selectedProjectEngineering: {},

  // customer
  customerProjects: [],
  customerQuotations: [],
  customerSelectedProject: {},
  // head project
  selectedRab: {},
  selectedPekerjaans: [],

  // ws project
  selectedWos: [],
  selectedWo: {},
  // skl
  selectedSkls: [],
  selectedSkl: {},
})

export const mutations = {
  SET_SELECTED_SKLS(state, skls) {
    state.selectedSkls = skls
  },
  SET_SELECTED_SKL(state, skl) {
    state.selectedSkl = skl
  },
  SET_SELECTED_PROJECT(state, project) {
    state.selectedProject = project
  },
  // engineering
  SET_SELECTED_WO_PROJECT(state, wo) {
    state.selectedWo = wo
  },
  SET_SELECTED_WOS(state, wos) {
    state.selectedWos = wos
  },
  SET_SELECTED_PEKERJAANS(state, pekerjaans) {
    state.selectedPekerjaans = pekerjaans
  },
  SET_SELECTED_RAB(state, rab) {
    state.selectedRab = rab
  },
  SET_SELECTED_PROJECT_ENGINEERING(state, project) {
    state.selectedProjectEngineering = project
  },
  // common
  SET_SELECTED_PROJECTS(state, payload) {
    state.selectedProjects = payload
  },
  RESET_SELECTED_PROJECTS(state, payload) {
    state.selectedProjects = []
  },
  // customer
  SET_CUSTOMER_SELECTED_PROJECT(state, payload) {
    state.selectedProject = payload
  },
  SET_CUSTOMER_PROJECTS(state, payload) {
    state.customerProjects = payload
  },
  RESET_CUSTOMER_PROJECTS(state) {
    state.customerProjects = []
  },
  SET_CUSTOMER_QUOTATIONS(state, payload) {
    state.customerQuotations = payload
  },
  RESET_CUSTOMER_QUOTATIONS(state) {
    state.customerQuotations = []
  },
  // estimator
  SET_PROJECTS(state, projects) {
    state.db_projects = projects
  },
  ADD_PROJECT(state, project) {
    const projectId = project.id
    let idx
    for (let i = 0; i < state.db_projects.length; i++) {
      const p = state.db_projects[i]
      if (p.id === projectId) {
        idx = i
      }
    }

    if (idx === 0 || idx > 0) {
      console.log('Project sudah ada ')
    } else {
      state.db_projects.push(project)
    }
  },
  UPDATE_PROJECT(state, project) {
    const projectId = project.id

    let idx
    for (let i = 0; i < state.db_projects.length; i++) {
      const p = state.db_projects[i]
      if (p.id === projectId) {
        idx = i
      }
    }

    state.db_projects.splice(idx, 1, project)
  },
  REMOVE_PROJECT(state, project) {
    const projectId = project.id

    let idx
    for (let i = 0; i < state.db_projects.length; i++) {
      const p = state.db_projects[i]
      if (p.id === projectId) {
        idx = i
      }
    }

    state.db_projects.splice(idx, 1)
  },
  SELECT_PROJECT(state, project) {
    state.selectedProject = project
  },
  CLEAR_SELECTED_PROJECT(state) {
    state.selectedProject = {}
  },
  SELECT_PROJECT_MANAGEMENT(state, project) {
    state.selectedProject = project
  },
  SELECT_PROJECT_PPIC(state, project) {
    state.selectedProject = project
  },
  UPDATE_MARGIN(state, margin) {
    state.selectedProjectManagement.margin = margin
  },
  UPDATE_RETENSI(state, retensi) {
    state.selectedProjectManagement.retensi = retensi
  },
  UPDATE_PAJAK_RETENSI(state, pajak) {
    state.selectedProjectManagement.pajak_retensi = pajak
  },
  CALCULATE_PROJECT_TOTAL(state) {
    // calculate nominal margin
    // calculate after margin
    // calculate nominal retensi
    // calculate nominal pajak retensi
    // calculate nilai_so_material
    // calculate nilai_so_project
    const nominal_margin =
      (state.selectedProjectManagement.margin *
        state.selectedProjectManagement.total_nilai_project) /
      100

    state.selectedProjectManagement.nominal_margin = nominal_margin
    console.log('nominal margin', nominal_margin)

    state.selectedProjectManagement.after_margin =
      state.selectedProjectManagement.total_nilai_project - nominal_margin

    const nominal_retensi =
      (state.selectedProjectManagement.after_margin *
        state.selectedProjectManagement.retensi) /
      100

    console.log('nominal_retensi', nominal_retensi)

    state.selectedProjectManagement.nominal_retensi = nominal_retensi

    const after_retensi =
      state.selectedProjectManagement.after_margin - nominal_retensi

    const nominal_pajak_retensi =
      (nominal_retensi * state.selectedProjectManagement.retensi) / 100

    state.selectedProjectManagement.nominal_pajak_retensi = nominal_pajak_retensi

    const after_pajak = after_retensi - nominal_pajak_retensi

    const nominal_so_material =
      (state.selectedProjectManagement.prosentase_material * after_pajak) / 100

    const nominal_so_project =
      (state.selectedProjectManagement.prosentase_project * after_pajak) / 100

    console.log('after_retensi', after_retensi)
    console.log('nominal_pajak_retensi', nominal_pajak_retensi)
    console.log('after_pajak', after_pajak)
    console.log('nominal_so_material', nominal_so_material)
    console.log('nominal_so_project', nominal_so_project)

    state.selectedProjectManagement.nilai_so_material = nominal_so_material
    state.selectedProjectManagement.nilai_so_project = nominal_so_project
  },
  CONFIRM_MARGIN(state) {
    state.selectedProjectManagement.status = 'Approved'
    state.selectedProjectManagement.status_level = 6
    state.selectedProjectManagement.status_text = 'Approved by Management'
  },
}

export const actions = {
  // common
  /**
   * get selected projects
   * todos add filter by status
   * @param {*} param0
   */
  async getSelectedProjects({ commit }) {
    console.info(`project.action.getSelectedProjects`)
    commit('RESET_SELECTED_PROJECTS')
    const projects = await this.$axios.$get(`/api/projects`)
    if (projects.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: projects.msg,
        })
      })
    } else {
      console.info(`project.action.getSelectedProjects data`, projects.data)
      commit('SET_SELECTED_PROJECTS', projects.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  // ppic
  async getPpicProjects({ commit }) {
    console.info('project.action.getPpicProjects')
    const projects = await this.$axios.$get('/api/projects/ppic')
    if (projects) {
      if (projects.error) {
        commit('RESET_PROJECTS')
      } else {
        commit('SET_PROJECTS', projects.data)
      }
    } else {
      commit('RESET_PROJECTS')
    }
  },
  // management
  async getManagementProjects({ commit, dispatch }) {
    console.info('project.action.getManagementProjects')
    const projects = await this.$axios.$get('/api/projects/management')
    if (projects) {
      if (projects.error) {
        commit('RESET_PROJECTS')
      } else {
        commit('SET_PROJECTS', projects.data)
        // can use this, because it set the same selectedQuotations
        dispatch('quotation/getCustomerQuotations', projects.data, {
          root: true,
        })
      }
    }
  },
  // customer
  selectCustomerProject({ commit }, payload) {
    commit('SET_CUSTOMER_SELECTED_PROJECT', payload)
  },
  clearCustomerProject({ commit }) {
    commit('RESET_CUSTOMER_SELECTED_PROJECT')
  },
  /**
   * get projects list for customer
   * return promise
   */
  async getCustomerProjects({ commit, dispatch }) {
    console.info('action: getCustomerProjects')
    // todos add customer code filter
    const projects = await this.$axios.$get('/api/projects/customer')
    if (projects) {
      if (projects.error) {
        commit('RESET_PROJECTS', projects.data)
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: projects.msg,
          })
        })
      } else {
        commit('SET_PROJECTS', projects.data)
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },

  setCustomerProject({ commit }, payload) {
    commit('SET_CUSTOMER_PROJECTS', payload)
  },
  resetCustomerProject({ commit }) {
    commit('RESET_CUSTOMER_PROJECTS')
  },
  // estimator
  setProject({ commit }, projects) {
    commit('SET_PROJECTS', projects)
  },
  /**
   * add new project
   * @param {*} param0
   * @param {*} project
   */
  async addProject({ dispatch }, project) {
    if (project) {
      const result = await this.$axios.$post('/api/projects', project)
      if (result.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: result.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  updateProject({ commit }, project) {
    commit('UPDATE_PROJECT', project)
  },
  removeProject({ commit }, project) {
    commit('REMOVE_PROJECT', project)
  },
  /**
   * set global state project.selectedProject
   * @param {*} param0
   * @param {*} project
   */
  selectProject({ commit }, project) {
    commit('SELECT_PROJECT', project)
  },
  selectProjectManagement({ commit, dispatch }, project) {
    commit('SELECT_PROJECT_MANAGEMENT', project)
    return dispatch('quotation/selectManagementQuotation', project, {
      root: true,
    })
  },
  /**
   * set selected project for engineering user
   * return promise
   * @param {*} param0
   * @param {*} project
   */
  async selectProjectEngineering({ commit, dispatch }, project) {
    // CHECK FOR RAB IN DATABASE
    commit('SET_SELECTED_PROJECT_ENGINEERING', project)
    return new Promise((resolve, reject) => {
      return resolve({
        error: false,
        msg: 'OK',
      })
    })
    // dispatch('rab/setSelectedRab', project, { root: true })
  },
  async selectProjectPpic({ commit, dispatch }, project) {
    // CHECK FOR RAB IN DATABASE
    commit('SELECT_PROJECT_PPIC', project)
    dispatch('rab/setSelectedRab', project, { root: true })
  },
  async updateMargin({ commit, dispatch }, payload) {
    const { id, margin } = payload
    if (id) {
      await this.$axios
        .$put(`/api/projects/update_margin/${id}`, {
          margin: margin,
        })
        .then((result) => {
          console.info('result', result)
          if (result.error) {
            dispatch('showError', result.msg, { root: true })
          } else {
            commit('SELECT_PROJECT', result.data)
          }
        })
    }
  },
  async updateRetensi({ commit, dispatch }, payload) {
    const { id, retensi } = payload
    if (id) {
      await this.$axios
        .$put(`/api/projects/update_retensi/${id}`, {
          retensi: retensi,
        })
        .then((result) => {
          console.info('result', result)
          if (result.error) {
            dispatch('showError', result.msg, { root: true })
          } else {
            commit('SELECT_PROJECT', result.data)
          }
        })
    }
  },
  async updatePajakRetensi({ commit, dispatch }, payload) {
    const { id, pajak } = payload
    if (id) {
      await this.$axios
        .$put(`/api/projects/update_pajak_retensi/${id}`, {
          pajak: pajak,
        })
        .then((result) => {
          console.info('result', result)
          if (result.error) {
            dispatch('showError', result.msg, { root: true })
          } else {
            commit('SELECT_PROJECT', result.data)
          }
        })
    }
  },
  calculateProjectTotal({ commit }) {
    commit('CALCULATE_PROJECT_TOTAL')
  },
  async konfirmMargin({ commit, dispatch }, payload) {
    const { id } = payload
    if (id) {
      await this.$axios
        .$put(`/api/projects/confirm_margin/${id}`, {
          confirm: 'OK',
        })
        .then((result) => {
          console.info('result', result)
          if (result.error) {
            dispatch('showError', result.msg, { root: true })
          } else {
            commit('SELECT_PROJECT', result.data)
          }
        })
    }
  },
  /**
   * delete project from database
   * return promise
   * @param {*} param0
   * @param {*} project
   */
  async deleteProject({ commit }, project) {
    console.info(`project.action.deleteProject`, project)
    const id = project.id
    if (id) {
      const d = await this.$axios.$delete(`/api/projects/${id}`)
      if (d.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: d.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: d.msg,
          })
        })
      }
    }
  },
  // head of project
  async getProjectsApproved({ commit }) {
    console.info('project.actions.getProjectsApproved')
    try {
      commit('RESET_SELECTED_PROJECTS')
      const projects = await this.$axios.$get(`/api/projects/project`)
      console.info('projects', projects)
      if (projects.error) {
        return rejected(projects.msg)
      } else {
        commit('SET_SELECTED_PROJECTS', projects.data)
        return resolved(projects.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async getRabProjectByProjectId({ commit }, id) {
    console.info('project.actions.getRabProjectByProjectId', id)
    if (!id) {
      return rejected('Parameter id tidak ditemukan')
    }

    try {
      const rab = await this.$axios.$get(`/api/rabps/project/${id}`)
      console.info('store rab', rab)
      if (rab.error) {
        throw rab.msg
      } else {
        commit('SET_SELECTED_RAB', rab.data)
        return resolved(rab.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async getPekerjaansByRabId({ commit }, id) {
    console.info('project.actions.getPekerjaansByRabId', id)
    if (!id) {
      return rejected('Parameter id tidak ditemukan')
    }

    try {
      const rab = await this.$axios.$get(`/api/rabps/pekerjaans/${id}`)
      if (rab.error) {
        throw rab.msg
      } else {
        commit('SET_SELECTED_PEKERJAANS', rab.data)
        return resolved(rab.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async updateRabPekerjaanHarga({ commit }, payload) {
    const { id, harga } = payload

    if (!id || !harga) {
      return rejected('Parameter id dan harga tidak valid')
    }

    try {
      const u = await this.$axios.$put(`/api/rabps/pekerjaan/${id}`, payload)
      console.info('u action', u)
      if (u.error) {
        return rejected(u.msg)
      } else {
        return resolved(u.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async addPekerjaanDetailRabProject({ comit }, payload) {
    const { parent_id } = payload

    if (!parent_id) {
      return rejected('Parameter id tidak ditemukan')
    }

    try {
      const a = await this.$axios.$post(`/api/rabps/detail/${parent_id}`, payload)
      if (a.error) {
        return rejected(a.msg)
      } else {
        return resolved(a.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async getRabPekerjaanDetailsByPekerjaanId({ commit }, id) {
    console.info('project.action.getRabPekerjaanDetailsByPekerjaanId', id)
    if (!id) {
      return rejected('Parameter id tidak ditemukan')
    }

    try {
      const details = await this.$axios.$get(`/api/rabps/details/${id}`)
      console.info('details', details)
      if (details.error) {
        return rejected(details.msg)
      } else {
        return resolved(details.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async removeDetailPekerjaanRab({ commit }, id) {
    if (!id) {
      return rejected('Parameter id tidak ditemukan')
    }

    try {
      const d = await this.$axios.$delete(`/api/rabps/delete_detail/${id}`)
      if (d.error) {
        return rejected(d.msg)
      } else {
        return resolved(d.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async addPekerjaanRabProject({ commit }, payload) {
    if (!payload) {
      return rejected('Parameter tidak ditemukan')
    }

    try {
      const a = await this.$axios.$post(`/api/rabps/pekerjaan`, payload)
      if (a.error) {
        return rejected(a.msg)
      } else {
        return resolved(a.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async confirmRabProject({ commit }, id) {
    if (!id) {
      return rejected('Parameter id tidak ditemukan ')
    }

    try {
      const c = await this.$axios.$put(`/api/rabps/confirm_rab/${id}`)
      if (c.error) {
        return rejected(c.msg)
      } else {
        return resolved(c.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async deletePekerjaan({ commit }, id) {
    if (!id) {
      return rejected('Parameter id tidak ditemukan')
    }

    try {
      const d = await this.$axios.$delete(`/api/rabps/delete_pekerjaan/${id}`)
      if (d.error) {
        return rejected(d.msg)
      } else {
        return resolved(d.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async getWOProjectsDraf({ commit }) {
    try {
      const wos = await this.$axios.$get(`/api/wos/draf`)
      if (wos.error) {
        return rejected(wos.msg)
      } else {
        commit('SET_SELECTED_WOS', wos.data)
        return resolved(wos.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  selectWoProject({ commit }, wo) {
    commit('SET_SELECTED_WO_PROJECT', wo)
  },
  async getProjectById({ commit }, id) {
    console.info('project.action.getProjectById', id)
    if (!id) {
      return rejected('Parameter id tidak ditemukan')
    }

    try {
      const project = await this.$axios.$get(`/api/projects/${id}`)
      if (project.error) {
        return rejected(project.msg)
      } else {
        commit('SET_SELECTED_PROJECT', project.data)
        return resolved(project.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async getPekerjaansWoByWoId({ commit }, id) {
    console.info('project.action.getPekerjaansWoByWoId', id)
    if (!id) {
      return rejected('Parameter id tidak ditemukan')
    }

    try {
      const pekerjaans = await this.$axios.$get(`/api/wos/pekerjaans/${id}`)
      if (pekerjaans.error) {
        return rejected(pekerjaans.msg)
      } else {
        commit('SET_SELECTED_PEKERJAANS', pekerjaans.data)
        return resolved(pekerjaans.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async getWoPekerjaanDetailsByPekerjaanId({ commit }, id) {
    console.info('project.action.getWoPekerjaanDetailsByPekerjaanId', id)
    if (!id) {
      return rejected('Parameter id tidak ditemukan')
    }

    try {
      const details = await this.$axios.$get(`/api/wos/details/${id}`)
      if (details.error) {
        return rejected(details.msg)
      } else {
        return resolved(details.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async updateHargaPekerjaanDetailWo({ commit }, payload) {
    const { id, harga } = payload
    if (!id) {
      return rejected('Parameter id tidak ditemukan ')
    }

    try {
      const u = await this.$axios.$put(`/api/wos/update_detail/${id}`, payload)
      if (u.error) {
        return rejected(u.msg)
      } else {
        return resolved(u.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async removeDetailPekerjaanWo({ commit }, id) {
    console.info('project.action.removeDetailPekerjaanWo')
    if (!id) {
      return rejected('Parameter id tidak ditemukan')
    }

    try {
      const d = await this.$axios.$delete(`/api/wos/delete_pekerjaan_detail/${id}`)
      if (d.error) {
        return rejected(d.msg)
      } else {
        return resolved(d.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async addPekerjaanDetailWoProject({ commit }, payload) {
    const { parent_id } = payload
    if (!parent_id) {
      return rejected('Parameter parent id tidak ditemukan')
    }

    try {
      const a = await this.$axios.$post(`/api/wos/detail/${parent_id}`, payload)
      if (a.error) {
        return rejected(a.msg)
      } else {
        return resolved(a.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async addPekerjaanWoProject({ commit }, payload) {
    if (!payload) {
      return rejected('Parameter data tidak ditemukan')
    }

    const { wo_id } = payload

    try {
      const a = await this.$axios.$post(`/api/wos/pekerjaan/${wo_id}`, payload)
      if (a.error) {
        return rejected(a.msg)
      } else {
        return resolved(a.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async getWoProjectById({ commit }, id) {
    if (!id) {
      return rejected('Parameter id tidak ditemukan')
    }

    try {
      const wo = await this.$axios.$get(`/api/wos/${id}`)
      if (wo.error) {
        return rejected(wo.msg)
      } else {
        commit('SET_SELECTED_WO_PROJECT', wo.data)
        return resolved(wo.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async confirmWo({ commit }, id) {
    if (!id) {
      return rejected('Parameter id tidak ditemukan ')
    }

    try {
      const c = await this.$axios.$put(`/api/wos/confirm_wo/${id}`)
      if (c.error) {
        return rejected(c.msg)
      } else {
        return resolved(c.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async getWOProjectsApproved({ commit }) {
    try {
      const wos = await this.$axios.$get(`/api/wos/approved`)
      if (wos.error) {
        return rejected(wos.msg)
      } else {
        commit('SET_SELECTED_WOS', wos.data)
        return resolved(wos.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async getSklsByWoId({ commit }, id) {
    if (!id) {
      return rejected('Parameter id tidak ditemukan')
    }

    try {
      const skls = await this.$axios.$get(`/api/wos/skls/${id}`)
      if (skls.error) {
        return rejected(skls.msg)
      } else {
        commit('SET_SELECTED_SKLS', skls.data)
        return resolved(skls.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async getSklById({ commit }, id) {
    if (!id) {
      return rejected('Parameter id tidak ditemukan')
    }

    try {
      const skls = await this.$axios.$get(`/api/wos/skl/${id}`)
      if (skls.error) {
        return rejected(skls.msg)
      } else {
        commit('SET_SELECTED_SKL', skls.data)
        return resolved(skls.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  selectSkl({ commit }, skl) {
    commit('SET_SELECTED_SKL', skl)
  },
  async addNewSKLByWoId({ commit }, id) {
    if (!id) {
      return rejected('Parameter id tidak ditemukan')
    }

    try {
      const a = await this.$axios.$get(`/api/wos/add_new_skl/${id}`)
      console.info(a)
      if (a.error) {
        return rejected(a.msg)
      } else {
        return resolved(a.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },

  async addPekerjaanSkl({ commit }, payload) {
    console.info('project.action.addPekerjaanSkl', payload)
    const { skl_id } = payload
    if (!skl_id) {
      return rejected('Parameter id tidak ditemukan ')
    }

    try {
      const a = await this.$axios.$post(`/api/wos/skl/${skl_id}`, payload)
      if (a.error) {
        return rejected(a.msg)
      } else {
        return resolved(a.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async getPekerjaansSklBySklId({ commit }, id) {
    console.info('project.action.getPekerjaansSklBySklId', id)
    if (!id) {
      return rejected('Parameter id tidak ditemukan')
    }

    try {
      const pekerjaans = await this.$axios.$get(`/api/wos/pekerjaans_skl/${id}`)
      if (pekerjaans.error) {
        return rejected(pekerjaans.msg)
      } else {
        return resolved(pekerjaans.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async deletePekerjaanSkl({ commit }, id) {
    if (!id) {
      return rejected('Parameter id tidak ditemukan')
    }

    try {
      const d = await this.$axios.$delete(`/api/wos/delete_pekerjaan_skl/${id}`)
      if (d.error) {
        return rejected(d.msg)
      } else {
        return resolved(d.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async confirmSKL({ commit }, id) {
    if (!id) {
      return rejected('Parameter id tidak ditemukan ')
    }

    try {
      const c = await this.$axios.$put(`/api/wos/confirm_skl/${id}`)
      if (c.error) {
        return rejected(c.msg)
      } else {
        return resolved(c.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async addRealisasiSkl({ commit }, payload) {
    const { skl_id, qty } = payload
    if (!skl_id) {
      return rejected('Parameter id tidak ditemukan')
    }

    try {
      await this.$axios
        .$post(`/api/wos/add_realisasi/${skl_id}`, payload)
        .then((result) => {
          console.info('result', result)
        })
        .catch((e) => {
          throw e.msg
        })
    } catch (error) {
      return rejected(error)
    }
  },
  async getSklRealisasiById({ commit }, id) {
    if (!id) {
      return rejected('Parameter id tidak ditemukan ')
    }

    try {
      const c = await this.$axios.$get(`/api/wos/history_realisasi/${id}`)
      if (c.error) {
        return rejected(c.msg)
      } else {
        return resolved(c.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
  async addSklAttachment({ commit }, payload) {
    const { skl_id, qty } = payload
    if (!skl_id) {
      return rejected('Parameter id tidak ditemukan')
    }

    try {
      return await this.$axios
        .$post(`/api/wos/add_attachment/${skl_id}`, payload)
        .then((result) => {
          console.info('action result', result)
          if (result.error) {
            throw result.msg
          } else {
            return resolved(result.data)
          }
        })
        .catch((e) => {
          throw e.msg
        })
    } catch (error) {
      return rejected(error)
    }
  },
  async getSklAttachmentById({ commit }, id) {
    if (!id) {
      return rejected('Parameter id tidak ditemukan ')
    }

    try {
      const c = await this.$axios.$get(`/api/wos/attachments/${id}`)
      if (c.error) {
        return rejected(c.msg)
      } else {
        return resolved(c.data)
      }
    } catch (error) {
      return rejected(error)
    }
  },
}
