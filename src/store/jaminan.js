export const state = () => ({
  db_jaminans: [],
  selectedJaminan: {},
  selectedJaminans: [],
})

export const mutations = {
  SET_SELECTED_JAMINANS(state, payload) {
    state.selectedJaminans = payload
  },
  RESET_SELECTED_JAMINANS(state) {
    state.selectedJaminans = []
  },
  SET_SELECTED_JAMINANS(state, jaminans) {
    state.selectedJaminans = jaminans
  },
  ADD_JAMINAN(state, jaminan) {
    const jaminanId = jaminan.id
    let idx
    for (let i = 0; i < state.db_jaminans.length; i++) {
      const p = state.db_jaminans[i]
      if (p.id === jaminanId) {
        idx = i
      }
    }

    if (idx === 0 || idx > 0) {
      console.log('Jaminan sudah ada ')
    } else {
      state.db_jaminans.push(jaminan)
    }
  },
  UPDATE_JAMINAN(state, jaminan) {
    const jaminanId = jaminan.id

    let idx
    for (let i = 0; i < state.db_jaminans.length; i++) {
      const p = state.db_jaminans[i]
      if (p.id === jaminanId) {
        idx = i
      }
    }

    state.db_jaminans.splice(idx, 1, jaminan)
  },
  REMOVE_JAMINAN(state, jaminan) {
    const jaminanId = jaminan.id

    let idx
    for (let i = 0; i < state.db_jaminans.length; i++) {
      const p = state.db_jaminans[i]
      if (p.id === jaminanId) {
        idx = i
      }
    }

    state.db_jaminans.splice(idx, 1)
  },
  SELECT_JAMINAN(state, jaminan) {
    state.selectedJaminan = jaminan
  },
  CLEAR_SELECTED_JAMINAN(state) {
    state.selectedJaminan = {}
  },
}

export const actions = {
  /**
   * get jaminan by quotation id
   * set global state jaminan.selectedJaminans
   * return promise
   * @param {*} param0
   * @param {*} id
   */
  async getSelectedJaminans({ commit }, id) {
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    } else {
      commit('RESET_SELECTED_JAMINANS')
      const js = await this.$axios.$get(`/api/jaminans/quotation/${id}`)
      if (js.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: js.msg,
          })
        })
      } else {
        commit('SET_SELECTED_JAMINANS', js.data)
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  setSelectedJaminans({ commit }, jaminans) {
    commit('SET_SELECTED_JAMINANS', jaminans)
  },
  /**
   * add new Jaminan
   * return promise
   * @param {*} param0
   * @param {*} jaminan
   */
  async addJaminan({ commit }, jaminan) {
    if (!jaminan) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter jamian tidak ditemukan',
        })
      })
    } else {
      const result = await this.$axios.$post('/api/jaminans', jaminan)
      if (result.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: result.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  updateJaminan({ commit }, jaminan) {
    commit('UPDATE_JAMINAN', jaminan)
  },
  /**
   * remove jaminan
   * return promise
   * @param {*} param0
   * @param {*} jaminan
   */
  async removeJaminan({ commit }, jaminan) {
    if (!jaminan) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter jaminan not found',
        })
      })
    } else {
      const id = jaminan.id
      const d = await this.$axios.$delete(`/api/jaminans/${id}`)
      if (d.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: d.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  selectJaminan({ commit }, jaminan) {
    commit('SELECT_JAMINAN', jaminan)
  },
}
