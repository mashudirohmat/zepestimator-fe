export const state = () => ({
  db_barangs: [],
  selectedBarang: {},
  selectedBarangs: [],
})

export const mutations = {
  SET_SELECTED_BARANGS(state, payload) {
    state.selectedBarangs = payload
  },
  RESET_SELECTED_BARANGS(state) {
    state.selectedBarangs = []
  },
  ADD_BARANG(state, barang) {
    const barangId = barang.id.length
    let idx
    for (let i = 0; i < state.db_barangs.length; i++) {
      const p = state.db_barangs[i]
      if (p.id === barangId) {
        idx = i
      }
    }

    if (idx === 0 || idx > 0) {
      console.log('Barang sudah ada ')
    } else {
      state.db_barangs.push(barang)
    }
  },
  UPDATE_BARANG(state, barang) {
    const barangId = barang.id

    let idx
    for (let i = 0; i < state.db_barangs.length; i++) {
      const p = state.db_barangs[i]
      if (p.id === barangId) {
        idx = i
      }
    }

    state.db_barangs.splice(idx, 1, barang)
  },
  REMOVE_BARANG(state, barang) {
    const barangId = barang.id

    let idx
    for (let i = 0; i < state.db_barangs.length; i++) {
      const p = state.db_barangs[i]
      if (p.id === barangId) {
        idx = i
      }
    }

    state.db_barangs.splice(idx, 1)
  },
  SELECT_BARANG(state, barang) {
    state.selectedBarang = barang
  },
  CLEAR_SELECTED_BARANG(state) {
    state.selectedBarang = {}
  },
}

export const actions = {
  /**
   * get barangs by quotation id
   * set global state barangs.selectedBarangs
   * return promise
   * @param {*} param0
   * @param {*} id
   */
  async getSelectedBarangs({ commit }, id) {
    console.log(`barang.actions.getSelectedBarangs with ${id}`)
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    } else {
      commit('RESET_SELECTED_BARANGS')

      const bs = await this.$axios.$get(`/api/barangs/quotation/${id}`)
      if (bs.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: bs.msg,
          })
        })
      } else {
        commit('SET_SELECTED_BARANGS', bs.data)
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  setSelectedBarangs({ commit }, payload) {
    commit('SET_SELECTED_BARANGS', payload)
  },
  /**
   * add new barang
   * calculate total barang, total quotation
   * return promise
   * @param {*} param0
   * @param {*} barang
   */
  async addBarang({ dispatch }, barang) {
    console.info(`barang.action.addBarang()`, barang)
    if (!barang) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter barang tidak ditemukan',
        })
      })
    } else {
      const ad = await this.$axios.$post('/api/barangs', barang)
      if (ad.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: ad.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  updateBarang({ commit }, barang) {
    commit('UPDATE_BARANG', barang)
  },
  /**
   * remove barang by id
   * return promise
   * @param {*} param0
   * @param {*} id
   */
  async removeBarang({ commit }, id) {
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    } else {
      const d = await this.$axios.$delete(`/api/barangs/${id}`)
      if (d.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: d.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  selectBarang({ commit }, barang) {
    commit('SELECT_BARANG', barang)
  },
}
