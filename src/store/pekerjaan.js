export const state = () => ({
  db_pekerjaans: [],
  selectedPekerjaan: {},
  selectedPekerjaans: [],
})

export const mutations = {
  SET_SELECTED_PEKERJAANS(state, payload) {
    state.selectedPekerjaans = payload
  },
  RESET_SELECTED_PEKERJAANS(state) {
    state.selectedPekerjaans = []
  },
  ADD_PEKERJAAN(state, pekerjaan) {
    const pekerjaanId = pekerjaan.id
    let idx
    for (let i = 0; i < state.db_pekerjaans.length; i++) {
      const p = state.db_pekerjaans[i]
      if (p.id === pekerjaanId) {
        idx = i
      }
    }

    if (idx === 0 || idx > 0) {
      console.log('Pekerjaan sudah ada ')
    } else {
      state.db_pekerjaans.push(pekerjaan)
    }
  },
  UPDATE_PEKERJAAN(state, pekerjaan) {
    const pekerjaanId = pekerjaan.id

    let idx
    for (let i = 0; i < state.db_pekerjaans.length; i++) {
      const p = state.db_pekerjaans[i]
      if (p.id === pekerjaanId) {
        idx = i
      }
    }

    state.db_pekerjaans.splice(idx, 1, pekerjaan)
  },
  REMOVE_PEKERJAAN(state, pekerjaan) {
    const pekerjaanId = pekerjaan.id

    let idx
    for (let i = 0; i < state.db_pekerjaans.length; i++) {
      const p = state.db_pekerjaans[i]
      if (p.id === pekerjaanId) {
        idx = i
      }
    }

    state.db_pekerjaans.splice(idx, 1)
  },
  SELECT_PEKERJAAN(state, pekerjaan) {
    state.selectedPekerjaan = pekerjaan
  },
  CLEAR_SELECTED_PEKERJAAN(state) {
    state.selectedPekerjaan = {}
  },
}

export const actions = {
  /**
   * get pekerjaan by quoation id
   * set global state selectedPekerjaans
   * return promise
   * @param {*} param0
   * @param {*} id
   */
  async getSelectedPekerjaans({ commit }, id) {
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    } else {
      commit('RESET_SELECTED_PEKERJAANS')
      const ps = await this.$axios.$get(`/api/pekerjaans/quotation/${id}`)
      if (ps.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: ps.msg,
          })
        })
      } else {
        commit('SET_SELECTED_PEKERJAANS', ps.data)
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  setSelectedPekerjaans({ commit }, payload) {
    commit('SET_SELECTED_PEKERJAANS', payload)
  },
  resetSelectedPekerjaans({ commit }) {
    commit('RESET_SELECTED_PEKERJAANS')
  },
  /**
   * add pekerjaan di quotation
   * reload pekerjaan, reload quotation
   * return promise
   * @param {*} param0
   * @param {*} pekerjaan
   */
  async addPekerjaan({ dispatch }, pekerjaan) {
    if (!pekerjaan) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter pekerjaan tidak ditemukan',
        })
      })
    } else {
      const ad = await this.$axios.$post('/api/pekerjaans', pekerjaan)
      if (ad.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: 'Parameter pekerjaan tidak ditemukan',
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  updatePekerjaan({ commit }, pekerjaan) {
    commit('UPDATE_PEKERJAAN', pekerjaan)
  },
  async removePekerjaan({ dispatch }, pekerjaan) {
    if (pekerjaan) {
      return await this.$axios
        .$delete(`/api/pekerjaans/${pekerjaan.id}`)
        .then((results) => {
          if (results.error) {
            console.log('err', results.msg)
            return results
          } else {
            dispatch('getSelectedPekerjaans', pekerjaan.quotation_id)
            return results
          }
        })
    } else {
      return new Promise((resolve, reject) => {
        reject('Parameter barang tidak ditemukan ')
      })
    }
  },
  selectPekerjaan({ commit }, pekerjaan) {
    commit('SELECT_PEKERJAAN', pekerjaan)
  },
}
