export const state = () => ({
  db_quotations: [],
  selectedQuotation: {},
  selectedQuotations: [],

  selectedCustomerQuotation: {},
  previewQuotation: {},
})

export const mutations = {
  SET_PREVIEW_QUOTATION(state, payload) {
    state.previewQuotation = payload
  },
  // estimator
  SET_SELECTED_QUOTATIONS(state, payload) {
    state.selectedQuotations = payload
  },
  RESET_SELECTED_QUOTATIONS(state) {
    state.selectedQuotations = []
  },
  SET_QUOTATION(state, payload) {
    state.selectedQuotation = payload
  },
  ADD_QUOTATION(state, quotation) {
    const quotationId = quotation.id
    let idx
    for (let i = 0; i < state.db_quotations.length; i++) {
      const p = state.db_quotations[i]
      if (p.id === quotationId) {
        idx = i
      }
    }

    if (idx === 0 || idx > 0) {
      console.log('Quotation sudah ada ')
    } else {
      state.db_quotations.push(quotation)
    }
  },
  UPDATE_QUOTATION(state, quotation) {
    const quotationId = quotation.id

    let idx
    for (let i = 0; i < state.db_quotations.length; i++) {
      const p = state.db_quotations[i]
      if (p.id === quotationId) {
        idx = i
      }
    }

    state.db_quotations.splice(idx, 1, quotation)
  },
  REMOVE_QUOTATION(state, quotation) {
    const quotationId = quotation.id

    let idx
    for (let i = 0; i < state.db_quotations.length; i++) {
      const p = state.db_quotations[i]
      if (p.id === quotationId) {
        idx = i
      }
    }

    state.db_quotations.splice(idx, 1)
  },
  CALCULATE_QUOTATION_TOTAL(state, rootState) {
    // subtotal
    let totalBarang = 0
    const barangs = rootState.barang.db_barangs
    const quotationId = state.selectedQuotation.id
    console.log('barangs', barangs)
    for (let ib = 0; ib < barangs.length; ib++) {
      const barang = barangs[ib]
      if (barang.quotation_id === quotationId) {
        totalBarang = totalBarang + barang.total
      }
    }
    console.log('totalBarang', totalBarang)

    let totalPekerjaan = 0
    const pekerjaans = rootState.pekerjaan.db_pekerjaans
    for (let ip = 0; ip < pekerjaans.length; ip++) {
      const pekerjaan = pekerjaans[ip]
      if (pekerjaan.quotation_id === quotationId) {
        totalPekerjaan = totalPekerjaan + pekerjaan.total
      }
    }
    console.log('totalPekerjaan', totalPekerjaan)

    state.selectedQuotation.subtotal = totalBarang + totalPekerjaan

    const potongan = state.selectedQuotation.potongan
    const tax = state.selectedQuotation.tax

    let nominal_potongan = 0
    let nominal_tax = 0
    if (potongan > 0) {
      console.log('update potongan', potongan)
      nominal_potongan = (potongan / 100) * (totalBarang + totalPekerjaan)
      console.log('update nominal_potongan', nominal_potongan)
    }

    state.selectedQuotation.nominal_potongan = nominal_potongan
    state.selectedQuotation.after_potongan =
      totalBarang + totalPekerjaan - nominal_potongan

    if (tax > 0) {
      console.log('update tax', tax)
      nominal_tax = (tax / 100) * state.selectedQuotation.after_potongan
      console.log('update nominal_tax', nominal_tax)
    }

    state.selectedQuotation.nominal_tax = nominal_tax
    state.selectedQuotation.total_nilai_project =
      state.selectedQuotation.after_potongan + nominal_tax

    console.log('nominal_potongan', nominal_potongan)
    console.log('nominal_tax', nominal_tax)

    // prosentase
    const prosentase_material =
      (totalBarang / state.selectedQuotation.subtotal) * 100
    const prosentase_project =
      (totalPekerjaan / state.selectedQuotation.subtotal) * 100

    state.selectedQuotation.prosentase_material = prosentase_material
    state.selectedQuotation.prosentase_project = prosentase_project
  },
  UPDATE_POTONGAN(state, potongan) {
    state.selectedQuotation.potongan = potongan
  },
  UPDATE_TAX(state, tax) {
    state.selectedQuotation.tax = tax
  },
  KIRIM_PENAWARAN_KE_CUSTOMER(state, rootState) {
    console.log('kirim penawran ke customer')
    // kirim penawaran ke customer
    /**
     * save as quotation to newQuotation
     * update newQuotation
     * status_level = 2
     * status = nego
     * status_text = 'Menunggu Jawaban Customer'
     * note = nomor penawaran lama
     * replied_by_customer = 0
     * replied_by_admin = 1
     *
     * steps:
     * update old status = 'sent'
     * update old status_text = 'Menunggu Jawaban Customer'
     * update old status_level = 1
     * update old replied_by_customer = 0
     * update old replied_by_admin = 1
     */
    const newId = () => {
      var rdmString = ''
      for (
        ;
        rdmString.length < 15;
        rdmString += Math.random().toString(36).substr(2)
      );
      return rdmString.substr(0, 15)
    }

    console.log('this.selectedQuotation', state.selectedQuotation)
    const newQuotation = Object.assign({}, state.selectedQuotation)
    console.log('newQuotation', newQuotation)

    newQuotation.id = newId()
    newQuotation.status = 'nego'
    newQuotation.status_level = 2
    newQuotation.status_text = 'Menunggu Jawaban Customer'
    newQuotation.replied_by_customer = 0
    newQuotation.replied_by_vendor = 1
    newQuotation.note = `Penawaran dari ${state.selectedQuotation.id}`

    state.db_quotations.push(newQuotation)

    state.selectedQuotation.status_text = 'Menunggu jawaban Customer'
    state.selectedQuotation.status = 'sent'
    state.selectedQuotation.status_level = 1
    state.selectedQuotation.replied_by_customer = 0
    state.selectedQuotation.replied_by_vendor = 1

    // copas juga attachments, top, jaminan, barang, pekerjaan, details
    const attachments = rootState.attachment.db_attachments
    const tops = rootState.top.db_tops
    const jaminans = rootState.jaminan.db_jaminans
    const barangs = rootState.barang.db_barangs
    const details = rootState.detail.db_details
    const pekerjaans = rootState.pekerjaan.db_pekerjaans

    console.log('barangs', barangs)
    console.log('details', details)
    console.log('attachments', attachments)
    console.log('tops', tops)
    console.log('jaminans', jaminans)
    console.log('pekerjaans', pekerjaans)

    let attchs = []
    if (attachments.length > 0) {
      for (let x = 0; x < attachments.length; x++) {
        const at = attachments[x]
        if (at.quotation_id === state.selectedQuotation.id) {
          const no = Object.assign({}, at)
          console.log('attc', no)
          no.id = newId()
          no.quotation_id = newQuotation.id
          // rootState.attachment.db_attachments.push(no)
          attchs.push(no)
        }
      }
    }

    let atops = []
    if (tops.length > 0) {
      for (let x = 0; x < tops.length; x++) {
        const at = tops[x]
        if (at.quotation_id === state.selectedQuotation.id) {
          const no = Object.assign({}, at)
          no.id = newId()
          no.quotation_id = newQuotation.id
          // rootState.top.db_tops.push(no)
          console.log('top', no)
          atops.push(no)
        }
      }
    }

    let ajams = []
    if (jaminans.length > 0) {
      for (let x = 0; x < jaminans.length; x++) {
        const at = jaminans[x]
        if (at.quotation_id === state.selectedQuotation.id) {
          const no = Object.assign({}, at)
          no.id = newId()
          no.quotation_id = newQuotation.id
          // rootState.jaminan.db_jaminans.push(no)
          console.log('jaminan', no)
          ajams.push(no)
        }
      }
    }

    let apek = []
    if (pekerjaans.length > 0) {
      for (let x = 0; x < pekerjaans.length; x++) {
        const at = pekerjaans[x]
        console.log('at', at)
        console.log(state.selectedQuotation.id)
        if (at.quotation_id === state.selectedQuotation.id) {
          const no = Object.assign({}, at)
          no.id = newId()
          no.quotation_id = newQuotation.id
          // rootState.jaminan.db_jaminans.push(no)
          console.log('jaminan', no)
          apek.push(no)
        }
      }
    }

    console.log('barangs', barangs)
    console.log('details', details)
    let abar = []
    let adet = []

    if (barangs.length > 0) {
      for (let x = 0; x < barangs.length; x++) {
        const at = barangs[x]
        if (at.quotation_id === state.selectedQuotation.id) {
          const no = Object.assign({}, at)
          no.quotation_id = newQuotation.id
          no.id = newId()
          // detail

          if (details.length > 0) {
            for (let y = 0; y < details.length; y++) {
              const dt = details[y]
              if (dt.parent_id === at.id) {
                const dto = Object.assign({}, dt)
                dto.id = newId()
                dto.parent_id = no.id
                // rootState.detail.db_details.push(dto)
                adet.push(dto)
              }
            }
          }

          // rootState.barang.db_barangs.push(no)
          abar.push(no)
        }
      }
    }

    console.log(attchs)
    console.log(ajams)
    console.log(atops)
    console.log(abar)
    console.log(adet)
    console.log(pekerjaans)

    for (let i = 0; i < attchs.length; i++) {
      const o = attchs[i]
      rootState.attachment.db_attachments.push(o)
    }

    for (let i = 0; i < ajams.length; i++) {
      const o = ajams[i]
      rootState.jaminan.db_jaminans.push(o)
    }

    for (let i = 0; i < atops.length; i++) {
      const o = atops[i]
      rootState.top.db_tops.push(o)
    }

    for (let i = 0; i < apek.length; i++) {
      const o = apek[i]
      rootState.pekerjaan.db_pekerjaans.push(o)
    }

    for (let i = 0; i < abar.length; i++) {
      const o = abar[i]
      rootState.barang.db_barangs.push(o)
    }

    for (let i = 0; i < adet.length; i++) {
      const o = adet[i]
      rootState.detail.db_details.push(o)
    }

    // update status project
    const projects = rootState.project.db_projects
    let idx
    for (let i = 0; i < projects.length; i++) {
      const p = projects[i]
      if (p.id === state.selectedQuotation.project_id) {
        idx = i
      }
    }

    projects[idx].status = 'nego'
    projects[idx].status_level = 1
  },
  KIRIM_PENAWARAN_KE_VENDOR(state, rootState) {
    console.log('kirim penawran ke VENDOR')
    // kirim penawaran ke customer
    /**
     * save as quotation to newQuotation
     * update newQuotation
     * status_level = 2
     * status = nego
     * status_text = 'Menunggu Jawaban Customer'
     * note = nomor penawaran lama
     * replied_by_customer = 0
     * replied_by_admin = 1
     *
     * steps:
     * update old status = 'sent'
     * update old status_text = 'Menunggu Jawaban Customer'
     * update old status_level = 1
     * update old replied_by_customer = 0
     * update old replied_by_admin = 1
     */
    const newId = () => {
      var rdmString = ''
      for (
        ;
        rdmString.length < 15;
        rdmString += Math.random().toString(36).substr(2)
      );
      return rdmString.substr(0, 15)
    }

    console.log('this.selectedQuotation', state.selectedQuotation)
    const newQuotation = Object.assign({}, state.selectedQuotation)
    console.log('newQuotation', newQuotation)

    newQuotation.id = newId()
    newQuotation.status = 'nego'
    newQuotation.status_level = 3
    newQuotation.status_text = 'Menunggu Jawaban Vendor'
    newQuotation.replied_by_customer = 1
    newQuotation.replied_by_vendor = 0
    newQuotation.note = `Jawaban untuk ${state.selectedQuotation.id}`

    state.db_quotations.push(newQuotation)

    state.selectedQuotation.status_text = 'Menunggu jawaban Vendor'
    state.selectedQuotation.status = 'sent'
    state.selectedQuotation.status_level = 1
    state.selectedQuotation.replied_by_customer = 1
    state.selectedQuotation.replied_by_vendor = 0

    // copas juga attachments, top, jaminan, barang, pekerjaan, details
    const attachments = rootState.attachment.db_attachments
    const tops = rootState.top.db_tops
    const jaminans = rootState.jaminan.db_jaminans
    const barangs = rootState.barang.db_barangs
    const details = rootState.detail.db_details
    const pekerjaans = rootState.pekerjaan.db_pekerjaans

    console.log('barangs', barangs)
    console.log('details', details)
    console.log('attachments', attachments)
    console.log('tops', tops)
    console.log('jaminans', jaminans)
    console.log('pekerjaans', pekerjaans)

    let attchs = []
    if (attachments.length > 0) {
      for (let x = 0; x < attachments.length; x++) {
        const at = attachments[x]
        if (at.quotation_id === state.selectedQuotation.id) {
          const no = Object.assign({}, at)
          console.log('attc', no)
          no.id = newId()
          no.quotation_id = newQuotation.id
          // rootState.attachment.db_attachments.push(no)
          attchs.push(no)
        }
      }
    }

    let atops = []
    if (tops.length > 0) {
      for (let x = 0; x < tops.length; x++) {
        const at = tops[x]
        if (at.quotation_id === state.selectedQuotation.id) {
          const no = Object.assign({}, at)
          no.id = newId()
          no.quotation_id = newQuotation.id
          // rootState.top.db_tops.push(no)
          console.log('top', no)
          atops.push(no)
        }
      }
    }

    let ajams = []
    if (jaminans.length > 0) {
      for (let x = 0; x < jaminans.length; x++) {
        const at = jaminans[x]
        if (at.quotation_id === state.selectedQuotation.id) {
          const no = Object.assign({}, at)
          no.id = newId()
          no.quotation_id = newQuotation.id
          // rootState.jaminan.db_jaminans.push(no)
          console.log('jaminan', no)
          ajams.push(no)
        }
      }
    }

    let apek = []
    if (pekerjaans.length > 0) {
      for (let x = 0; x < pekerjaans.length; x++) {
        const at = pekerjaans[x]
        console.log('at', at)
        console.log(state.selectedQuotation.id)
        if (at.quotation_id === state.selectedQuotation.id) {
          const no = Object.assign({}, at)
          no.id = newId()
          no.quotation_id = newQuotation.id
          // rootState.jaminan.db_jaminans.push(no)
          console.log('jaminan', no)
          apek.push(no)
        }
      }
    }

    console.log('barangs', barangs)
    console.log('details', details)
    let abar = []
    let adet = []

    if (barangs.length > 0) {
      for (let x = 0; x < barangs.length; x++) {
        const at = barangs[x]
        if (at.quotation_id === state.selectedQuotation.id) {
          const no = Object.assign({}, at)
          no.quotation_id = newQuotation.id
          no.id = newId()
          // detail

          if (details.length > 0) {
            for (let y = 0; y < details.length; y++) {
              const dt = details[y]
              if (dt.parent_id === at.id) {
                const dto = Object.assign({}, dt)
                dto.id = newId()
                dto.parent_id = no.id
                // rootState.detail.db_details.push(dto)
                adet.push(dto)
              }
            }
          }

          // rootState.barang.db_barangs.push(no)
          abar.push(no)
        }
      }
    }

    console.log(attchs)
    console.log(ajams)
    console.log(atops)
    console.log(abar)
    console.log(adet)
    console.log(pekerjaans)

    for (let i = 0; i < attchs.length; i++) {
      const o = attchs[i]
      rootState.attachment.db_attachments.push(o)
    }

    for (let i = 0; i < ajams.length; i++) {
      const o = ajams[i]
      rootState.jaminan.db_jaminans.push(o)
    }

    for (let i = 0; i < atops.length; i++) {
      const o = atops[i]
      rootState.top.db_tops.push(o)
    }

    for (let i = 0; i < apek.length; i++) {
      const o = apek[i]
      rootState.pekerjaan.db_pekerjaans.push(o)
    }

    for (let i = 0; i < abar.length; i++) {
      const o = abar[i]
      rootState.barang.db_barangs.push(o)
    }

    for (let i = 0; i < adet.length; i++) {
      const o = adet[i]
      rootState.detail.db_details.push(o)
    }

    // update status project
    const projects = rootState.project.db_projects
    let idx
    for (let i = 0; i < projects.length; i++) {
      const p = projects[i]
      if ((p.id = state.selectedQuotation.project_id)) {
        idx = i
      }
    }

    projects[idx].status = 'nego'
    projects[idx].status_level = 3
  },
  SETUJU_PENAWARAN(state, rootState) {
    console.log('DEAL')
    // kirim penawaran ke customer
    /**
     * save as quotation to newQuotation
     * update newQuotation
     * status_level = 2
     * status = nego
     * status_text = 'Menunggu Jawaban Customer'
     * note = nomor penawaran lama
     * replied_by_customer = 0
     * replied_by_admin = 1
     *
     * steps:
     * update old status = 'sent'
     * update old status_text = 'Menunggu Jawaban Customer'
     * update old status_level = 1
     * update old replied_by_customer = 0
     * update old replied_by_admin = 1
     */
    const newId = () => {
      var rdmString = ''
      for (
        ;
        rdmString.length < 15;
        rdmString += Math.random().toString(36).substr(2)
      );
      return rdmString.substr(0, 15)
    }

    console.log('this.selectedQuotation', state.selectedQuotation)
    const newQuotation = Object.assign({}, state.selectedQuotation)
    console.log('newQuotation', newQuotation)

    newQuotation.id = newId()
    newQuotation.status = 'deal'
    newQuotation.status_level = 5
    newQuotation.status_text = 'Customer Setuju'
    newQuotation.replied_by_customer = 1
    newQuotation.replied_by_vendor = 0
    newQuotation.note = `Jawaban untuk ${state.selectedQuotation.id}`

    state.db_quotations.push(newQuotation)

    state.selectedQuotation.status_text = 'Customer Setuju'
    state.selectedQuotation.status = 'sent'
    state.selectedQuotation.status_level = 1
    state.selectedQuotation.replied_by_customer = 1
    state.selectedQuotation.replied_by_vendor = 0

    // copas juga attachments, top, jaminan, barang, pekerjaan, details
    const attachments = rootState.attachment.db_attachments
    const tops = rootState.top.db_tops
    const jaminans = rootState.jaminan.db_jaminans
    const barangs = rootState.barang.db_barangs
    const details = rootState.detail.db_details
    const pekerjaans = rootState.pekerjaan.db_pekerjaans

    console.log('barangs', barangs)
    console.log('details', details)
    console.log('attachments', attachments)
    console.log('tops', tops)
    console.log('jaminans', jaminans)
    console.log('pekerjaans', pekerjaans)

    let attchs = []
    if (attachments.length > 0) {
      for (let x = 0; x < attachments.length; x++) {
        const at = attachments[x]
        if (at.quotation_id === state.selectedQuotation.id) {
          const no = Object.assign({}, at)
          console.log('attc', no)
          no.id = newId()
          no.quotation_id = newQuotation.id
          // rootState.attachment.db_attachments.push(no)
          attchs.push(no)
        }
      }
    }

    let atops = []
    if (tops.length > 0) {
      for (let x = 0; x < tops.length; x++) {
        const at = tops[x]
        if (at.quotation_id === state.selectedQuotation.id) {
          const no = Object.assign({}, at)
          no.id = newId()
          no.quotation_id = newQuotation.id
          // rootState.top.db_tops.push(no)
          console.log('top', no)
          atops.push(no)
        }
      }
    }

    let ajams = []
    if (jaminans.length > 0) {
      for (let x = 0; x < jaminans.length; x++) {
        const at = jaminans[x]
        if (at.quotation_id === state.selectedQuotation.id) {
          const no = Object.assign({}, at)
          no.id = newId()
          no.quotation_id = newQuotation.id
          // rootState.jaminan.db_jaminans.push(no)
          console.log('jaminan', no)
          ajams.push(no)
        }
      }
    }

    let apek = []
    if (pekerjaans.length > 0) {
      for (let x = 0; x < pekerjaans.length; x++) {
        const at = pekerjaans[x]
        console.log('at', at)
        console.log(state.selectedQuotation.id)
        if (at.quotation_id === state.selectedQuotation.id) {
          const no = Object.assign({}, at)
          no.id = newId()
          no.quotation_id = newQuotation.id
          // rootState.jaminan.db_jaminans.push(no)
          console.log('jaminan', no)
          apek.push(no)
        }
      }
    }

    console.log('barangs', barangs)
    console.log('details', details)
    let abar = []
    let adet = []

    if (barangs.length > 0) {
      for (let x = 0; x < barangs.length; x++) {
        const at = barangs[x]
        if (at.quotation_id === state.selectedQuotation.id) {
          const no = Object.assign({}, at)
          no.quotation_id = newQuotation.id
          no.id = newId()
          // detail

          if (details.length > 0) {
            for (let y = 0; y < details.length; y++) {
              const dt = details[y]
              if (dt.parent_id === at.id) {
                const dto = Object.assign({}, dt)
                dto.id = newId()
                dto.parent_id = no.id
                // rootState.detail.db_details.push(dto)
                adet.push(dto)
              }
            }
          }

          // rootState.barang.db_barangs.push(no)
          abar.push(no)
        }
      }
    }

    console.log(attchs)
    console.log(ajams)
    console.log(atops)
    console.log(abar)
    console.log(adet)
    console.log(pekerjaans)

    for (let i = 0; i < attchs.length; i++) {
      const o = attchs[i]
      rootState.attachment.db_attachments.push(o)
    }

    for (let i = 0; i < ajams.length; i++) {
      const o = ajams[i]
      rootState.jaminan.db_jaminans.push(o)
    }

    for (let i = 0; i < atops.length; i++) {
      const o = atops[i]
      rootState.top.db_tops.push(o)
    }

    for (let i = 0; i < apek.length; i++) {
      const o = apek[i]
      rootState.pekerjaan.db_pekerjaans.push(o)
    }

    for (let i = 0; i < abar.length; i++) {
      const o = abar[i]
      rootState.barang.db_barangs.push(o)
    }

    for (let i = 0; i < adet.length; i++) {
      const o = adet[i]
      rootState.detail.db_details.push(o)
    }

    // update status project
    const projects = rootState.project.db_projects
    let idx
    for (let i = 0; i < projects.length; i++) {
      const p = projects[i]
      if ((p.id = state.selectedQuotation.project_id)) {
        idx = i
      }
    }

    projects[idx].status = 'deal'
    projects[idx].status_level = 5
    projects[idx].subtotal = newQuotation.subtotal
    projects[idx].potongan = newQuotation.potongan
    projects[idx].nominal_potongan = newQuotation.nominal_potongan
    projects[idx].tax = newQuotation.tax
    projects[idx].nominal_tax = newQuotation.nominal_tax
    projects[idx].total_nilai_project = newQuotation.total_nilai_project
    projects[idx].prosentase_material = newQuotation.prosentase_material
    projects[idx].prosentase_project = newQuotation.prosentase_project
  },
}

export const actions = {
  async getPreviewQuotationById({ commit }, id) {
    if (!id) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }

    let q = await this.$axios.$get('/api/quotations/preview', id)
    if (q.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: q.msg,
        })
      })
    } else {
      commit('SET_PREVIEW_QUOTATION', q.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  // management
  async selectManagementQuotation({ commit }, payload) {
    // select quotation with status_level 5 from  the project

    const { id } = payload
    console.info('quotation.action.selectManagementQuotation', id)

    const quotation = await this.$axios.$get(`/api/quotations/management/${id}`)
    if (quotation) {
      console.info('quotation', quotation)
      if (quotation.error) {
      } else {
        commit('SET_QUOTATION', quotation.data)
      }
    }
    return quotation
  },
  // customer
  selectCustomerQuotation({ commit }, payload) {
    commit('SET_QUOTATION', payload)
  },
  clearCustomerQuotation({ commit }) {
    commit('SET_QUOTATION')
  },
  /**
   * get quotations list for customer project
   */
  async getCustomerQuotations({ commit }, projects) {
    console.info('action: getCustomerQuotations', projects)
    const pjs = await this.$axios.$get('/api/projects/customer')
    if (pjs) {
      for (let i = 0; i < pjs.length; i++) {
        const item = pjs[i]
        const projectId = item.id
        const quotations = await this.$axios.$get(
          `/api/quotations/customer/${projectId}`
        )
        if (quotations.error) {
          commit('RESET_SELECTED_QUOTATIONS')
        } else {
          commit('SET_SELECTED_QUOTATIONS', quotations.data)
        }
      }
    }
  },
  // estimator
  /**
   * get selected quotation by project
   * return promise
   */
  async getSelectedQuotations({ commit }, payload) {
    if (!payload) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter project tidak ditemukan',
        })
      })
    }

    const { id } = payload
    commit('RESET_SELECTED_QUOTATIONS')
    const qs = await this.$axios.$get(`/api/quotations/project/${id}`)

    if (qs.error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: qs.msg,
        })
      })
    } else {
      commit('SET_SELECTED_QUOTATIONS', qs.data)
      return new Promise((resolve, reject) => {
        resolve({
          error: false,
          msg: 'OK',
        })
      })
    }
  },
  /**
   * get quotation by id
   * return promise
   */
  async getQuotationById({ commit }, id) {
    console.info('quotation.action.getQuotationById', id)
    console.info('typeof id', typeof id)
    console.info('!id', !id)

    try {
      if (id !== undefined && id !== null && id !== '') {
        const q = await this.$axios.$get(`/api/quotations/${id}`)

        if (q.error) {
          return new Promise((resolve, reject) => {
            reject({
              error: true,
              msg: q.msg,
            })
          })
        } else {
          commit('SET_QUOTATION', q.data)
          return new Promise((resolve, reject) => {
            resolve({
              error: false,
              msg: 'OK',
            })
          })
        }
      } else {
        console.info('getQuotationById')
        throw 'Parameter id tidak ditemukan (1)'
      }
    } catch (error) {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: error,
        })
      })
    }
  },
  /**
   * add new quotation to a project
   * return a promise
   */
  async addQuotation({ commit }, quotation) {
    if (quotation) {
      const result = await this.$axios.$post('/api/quotations', quotation)
      if (result.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: result.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    } else {
      return new Promise((resolve, reject) => {
        reject({
          error: true,
          msg: 'Parameter quotation tidak ditemukan',
        })
      })
    }
  },
  updateQuotation({ commit }, quotation) {
    commit('UPDATE_QUOTATION', quotation)
  },
  removeQuotation({ commit }, quotation) {
    commit('REMOVE_QUOTATION', quotation)
  },
  /**
   * set global state quotation.selectedQuotation
   * @param {*} param0
   * @param {*} quotation
   */
  selectQuotation({ commit }, quotation) {
    commit('SET_QUOTATION', quotation)
  },
  calculateQuotationTotal({ commit, rootState }) {
    commit('CALCULATE_QUOTATION_TOTAL', rootState)
  },
  /**
   * action: updatePotongan
   * return new Promise
   */
  async updatePotongan({ commit }, payload) {
    const { id, potongan } = payload
    if (!id) {
      return new Promise((resolve, reject) => {
        console.info('updatePotongan')
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    } else {
      const ud = await this.$axios.$put(`/api/quotations/updatePotongan/${id}`, {
        potongan: potongan,
      })
      if (ud.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: ud.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'ok',
          })
        })
      }
    }
  },
  async updateTax({ commit }, payload) {
    const { id, tax } = payload
    console.info('quotaion.action.updateTax', id)
    if (id !== null && id !== undefined && id !== '') {
      const ud = await this.$axios.$put(`/api/quotations/updateTax/${id}`, {
        tax: tax,
      })
      if (ud.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: ud.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'ok',
          })
        })
      }
    } else {
      return new Promise((resolve, reject) => {
        console.info('updateTax')
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }
  },
  async updateNote({ commit }, payload) {
    const { id, note } = payload
    if (id) {
      return await this.$axios.$put(`/api/quotations/updateNote/${id}`, {
        note: note,
      })
    } else {
      return new Promise((resolve, reject) => {
        console.info('updateNote')
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }
  },
  /**
   * update status quotation
   * save as quotation
   * return promise
   */
  async kirimPenawaranKeCustomer({ commit }, payload) {
    const { id, quotation } = payload
    console.info('quotation.action.kirimPenawaran', id)
    if (!id) {
      return new Promise((resolve, reject) => {
        console.info('kirimPenawaranKeCustomer')
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    } else {
      const ud = await this.$axios.$put(`/api/quotations/kirimPenawaran/${id}`, {
        quotation: quotation,
      })
      if (ud.error) {
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: ud.msg,
          })
        })
      } else {
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
  async kirimJawabanKeCustomer({ commit }, payload) {
    const { id, quotation } = payload
    if (id) {
      return await this.$axios.$put(`/api/quotations/kirimPenawaran/${id}`, {
        quotation: quotation,
      })
    } else {
      return new Promise((resolve, reject) => {
        console.info('kirimJawabanKeCustomer')
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }
  },

  async kirimPenawaranKeVendor({ commit }, payload) {
    const { id, quotation } = payload
    if (id) {
      return await this.$axios.$put(`/api/quotations/kirimJawaban/${id}`, {
        quotation: quotation,
      })
    } else {
      return new Promise((resolve, reject) => {
        console.info('kirimPenawaranKeVendor')
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }
  },
  async setujuPenawaran({ commit }, payload) {
    const { id, quotation } = payload
    if (id) {
      return await this.$axios.$put(`/api/quotations/setujuPenawaran/${id}`, {
        quotation: quotation,
      })
    } else {
      return new Promise((resolve, reject) => {
        console.info('setujuPenawaran')
        reject({
          error: true,
          msg: 'Parameter id tidak ditemukan',
        })
      })
    }
  },
  async getCustomerQuotationsAll({ commit }) {
    console.info('action: getCustomerQuotations')
    // todos add customer code filter
    const quotations = await this.$axios.$get('/api/quotations/customer_all')
    if (quotations) {
      if (quotations.error) {
        commit('RESET_SELECTED_QUOTATIONS')
        return new Promise((resolve, reject) => {
          reject({
            error: true,
            msg: quotations.msg,
          })
        })
      } else {
        commit('SET_SELECTED_QUOTATIONS', quotations.data)
        return new Promise((resolve, reject) => {
          resolve({
            error: false,
            msg: 'OK',
          })
        })
      }
    }
  },
}
